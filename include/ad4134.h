#ifndef LIGO_AD4134_H
#define LIGO_AD4134_H

#include <linux/types.h>


typedef struct ad4134_gain_t {
    uint8_t CH_GAIN_LSB;
    uint8_t CH_GAIN_MID;
    uint8_t CH_GAIN_MSB;

    uint8_t CH_OFFSET_LSB;
    uint8_t CH_OFFSET_MID;
    uint8_t CH_OFFSET_MSB;
} ad4134_gain_t;

typedef struct ad4134_regs_t
{
    uint8_t INTERFACE_CONFIG_A;
    uint8_t INTERFACE_CONFIG_B;
    uint8_t DEVICE_CONFIG;
    uint8_t CHIP_TYPE;
    uint8_t PRODUCT_ID_LSB;
    uint8_t PRODUCT_ID_MSB;
    uint8_t CHIP_GRADE;
    uint8_t SILICON_REV;
    uint8_t pad0[2];
    uint8_t SCRATCH_PAD;
    uint8_t SPI_REVISION;
    uint8_t VENDOR_ID_LSB;
    uint8_t VENDOR_ID_MSB;
    uint8_t STREAM_MODE;
    uint8_t TRANSFER_REGISTER;
    uint8_t DEVICE_CONFIG_1;
    uint8_t DATA_PACKET_CONFIG;
    uint8_t DIGITAL_INTERFACE_CONFIG;
    uint8_t POWER_DOWN_CONTROL;
    uint8_t RESERVED0;
    uint8_t DEVICE_STATUS;
    uint8_t ODR_VAL_INT_LSB;
    uint8_t ODR_VAL_INT_MID;
    uint8_t ODR_VAL_INT_MSB;
    uint8_t ODR_VAL_FLT_LSB;
    uint8_t ODR_VAL_FLT_MID0;
    uint8_t ODR_VAL_FLT_MID1;
    uint8_t ODR_VAL_FLT_MSB;
    uint8_t CHANNEL_ODR_SELECT;
    uint8_t CHAN_DIG_FILTER_SEL;
    uint8_t FIR_BW_SEL;
    uint8_t GPIO_DIR_CTRL;
    uint8_t GPIO_DATA;
    uint8_t ERROR_PIN_SRC_CONTROL;
    uint8_t ERROR_PIN_CONTROL;
    uint8_t VCMBUF_CTRL;
    uint8_t DIAGNOSTIC_CONTROL; 
    uint8_t MPC_CONFIG;

    ad4134_gain_t gain_settings[4];

    uint8_t MCLK_COUNTER;
    uint8_t DIG_FILTER_OFUF;
    uint8_t DIG_FILTER_SETTLED;
    uint8_t INTERNAL_ERROR;
    uint8_t pad1[4];
    uint8_t SPI_ERROR;
    uint8_t AIN_OR_ERROR; //Should be 0x48
 
} ad4134_regs_t;


#endif // LIGO_AD4134_H