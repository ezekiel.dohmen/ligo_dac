#ifndef LIGO_28AO32_H
#define LIGO_28AO32_H

#include <linux/types.h>
#include <linux/dma-mapping.h>

//PCI Subsystem Information
#define L28AO_VENDOR_ID 0x10EE
#define L28AO_DEVICE_ID 0xD8C7 

//DAC Constants
#define L28AO_SAMPLE_LIMIT 134217000
//Don't mask out any of the bit depth,
//while we can only use 28 bits, the DMAed 
//data should be sign extended out to 32 bit.
#define L28AO_SAMPLE_MASK 0xFFFFFFFF 
#define L28AO_NUM_CHANS 32

/**
* @brief The ligo28ao32's status codes, returned from various functions.
*
*/
typedef enum ligo28ao32_error_t
{
    LIGO32AO32_DAC_ERROR = -2, /**< The DAC has reported an error */
    LIGO32AO32_TIMEOUT = -1, /**< The operation requested timed out */
    LIGO32AO32_OK = 0, /**< Nominal function return */

} ligo28ao32_error_t;

//Forward declaration
typedef struct ligo28ao32_dev_t ligo28ao32_dev_t;

/**
* @brief Finds and configures the card instance requested.
*
* Finds the instance indexed ligo28ao32 on the PCIE bus,
* initializes it and returns a pointer to a context used
* for the other ligo28ao32_* functions.
*
* @param instance The PCIE bus order index of the card to configure, 
*                 0 first card, 1 second, etc.
*
* @return A pointer to the newly created context for this instance of the card,
*         or NULL if there was some error finding/configuring the card.
*         Pointer <b>must</b> be passed to ligo28ao32_freeDevice() when device
*         should be freed. 
*/
ligo28ao32_dev_t * ligo28ao32_configureDevice(unsigned instance);

/**
* @brief Cleans up the device and frees the context pointer passed
*
* @param dev A pointer to the device returned by ligo28ao32_configureDevice()
*
* @return None
*/
void ligo28ao32_freeDevice(ligo28ao32_dev_t * dev);

//
// General startup and configuration functions

/**
* @brief Queries the current GPS time of the device and returns it in ns.
*
* @param dev The pointer to the device context
*
* @return The current GPS time in ns.
*/
uint64_t ligo28ao32_get_time_ns(ligo28ao32_dev_t * dev);

/**
* @brief Queries the current GPS time of the device and returns it in sec.
*
* @param dev The pointer to the device context
*
* @return The current GPS time in floating point seconds.
*/
double ligo28ao32_get_time_sec(ligo28ao32_dev_t * dev);

/**
* @brief Queries the card the check if its timing is locked.
*
* The timing card needs to be configured to pass the timing signal
* to the converter board over the IO chassis backplane. 
* A starting place is section 7 of: https://dcc.ligo.org/LIGO-T2200212
*
* @param dev The pointer to the device context.
*
* @return 1 if the converted has its timing locked. 
*/
int ligo28ao32_is_locked(ligo28ao32_dev_t * dev);

/**
* @brief Queries the card and returns the slot number, if available.
*
* The timing card needs to receiving a timing signal from the 
* timing card for this index to be valid. Slot 0 is always the 
* timing master card, that will return -1. The valid converter
* card slots are 1,2,3...
*
* @param dev The pointer to the device context.
*
* @return The slot number, or -1 if unconfigured
*/
int ligo28ao32_get_backplane_slot(ligo28ao32_dev_t * dev);


//
//Start ADC Functions
//

//Non-locking ADC functions
/**
* @brief Reads the num_adc_channels register and returns the value.
*
* @param dev The pointer to the device context.
*
* @return The number of ADC channels reported in the converter's register
*/
int ligo28ao32_get_num_adc_chans(ligo28ao32_dev_t * dev);

/**
* @brief Reads the samp_status register and returns the value
*        of the bit describing if the ADC configuration is valid.
*
* @param dev The pointer to the device context.
*
* @return 1 if the ADC configuration is valid, 0 otherwise
*/
int ligo28ao32_is_adc_config_valid(ligo28ao32_dev_t * dev);

/**
* @brief Reads the samp_status register and returns the value
*        of the bit describing if the ADC DMA is running.
*
* @param dev The pointer to the device context.
*
* @return 1 if the ADC DMA is running, 0 otherwise
*/
int ligo28ao32_is_adc_dma_running(ligo28ao32_dev_t * dev);

/**
* @brief Reads the samp_status register and returns the value
*        of the bit describing if the ADC converter is running.
*
* @param dev The pointer to the device context.
*
* @return 1 if the ADC converter is running, 0 otherwise
*/
int ligo28ao32_is_adc_converter_running(ligo28ao32_dev_t * dev);

/**
* @brief Reads status field from the ADC DMA buffer and
*        returns the value. Section 5.1 in LIGO-T2200212
*        WARNING: The ADC buffer won't have a valid status until
*        an ADC DMA has been completed. 
*
* @param dev The pointer to the device context.
*
* @return The value of the status field in the ADC DMA buffer.
*/
uint32_t ligo28ao32_get_adc_buf_status(ligo28ao32_dev_t * dev);

/**
* @brief Marks the overflows field in the with all 1s so we
*        can detect when a new DMA has completed.
*
* @param dev The pointer to the device context.
*
* @return None.
*/
void ligo28ao32_consume_adc_buffer(ligo28ao32_dev_t * dev);

/**
* @brief Spins on the overflows field waiting for it to be overwritten 
*        by the ADC DMA, returns the GPS time (in ns) if the DMA was detected.
*
* @param dev The pointer to the device context.
* @param max_wait_us The max time this function will wait for the ADC DMA (in us)
* @param gps_ts_ns [out] A pointer that this function will fill with the GPS 
*                        time in the ADC DMA buffer.
*
* @return LIGO32AO32_OK if the next DMA was detected within the timeout, 
*         LIGO32AO32_TIMEOUT if the DMA was not detected before the timeout
*/
ligo28ao32_error_t ligo28ao32_wait_adc_dma(ligo28ao32_dev_t * dev, uint32_t max_wait_us, uint64_t * gps_ts_ns);

//Locking ADC functions
//
// The below 'locking' functions copy the ADC buffer into an auxiliary buffer to allow
// for processing without needing to worry about another ADC DMA overwriting the data
// The format of this buffer is documented in LIGO-T2200212, section 5.1
//

/**
* @brief Spins on the overflows field waiting for it to be overwritten 
*        by the ADC DMA, copies the DMA tranfer into an auxiliary buffer
*        if the DMA was detected.
*
* @param dev The pointer to the device context.
* @param max_wait_us The max time this function will wait for the ADC DMA (in us)
*
* @return LIGO32AO32_OK if the next DMA was detected within the timeout, 
*         LIGO32AO32_TIMEOUT if the DMA was not detected before the timeout
*/
ligo28ao32_error_t ligo28ao32_lock_next_adc_buffer(ligo28ao32_dev_t * dev, uint32_t max_wait_us);

/**
* @brief Retrieves the timestamp (in ns) of the ADC data 
*        from the auxiliary buffer
*
* @param dev The pointer to the device context.
*
* @return The timestamp of the stored ADC buffer. 
*/
uint64_t ligo28ao32_get_locked_ts_ns(ligo28ao32_dev_t * dev);

/**
* @brief Retrieves the raw timestamp of the ADC data from the auxiliary buffer
*
* @param dev The pointer to the device context.
*
* @return The timestamp of the stored ADC buffer.
*/
uint64_t ligo28ao32_get_locked_ts_raw(ligo28ao32_dev_t * dev);

/**
* @brief Retrieves the status field in the auxiliary buffer
*
* @param dev The pointer to the device context.
*
* @return The status field in the stored ADC buffer.
*/
uint32_t ligo28ao32_get_locked_status(ligo28ao32_dev_t * dev);

/**
* @brief Retrieves the overflow field in the auxiliary buffer
*
* @param dev The pointer to the device context.
*
* @return The overflow field in the stored ADC buffer.
*/
uint32_t ligo28ao32_get_locked_overflow(ligo28ao32_dev_t * dev);

/**
* @brief Retrieves a pointer to the samples in the auxiliary buffer
*
* @param dev The pointer to the device context.
*
* @return A pointer to the samples in the stored ADC buffer.
*/
int32_t * ligo28ao32_get_locked_samples_ptr(ligo28ao32_dev_t * dev);



//
//Start DAC Functions
//

/**
* @brief Reads the num_dac_channels register and returns the value.
*
* @param dev The pointer to the device context.
*
* @return The number of DAC channels reported in the converter's register
*/
int ligo28ao32_get_num_dac_chans(ligo28ao32_dev_t * dev);

/**
* @brief Reads the samp_status register and returns the value
*        of the bit describing if the DAC configuration is valid.
*
* @param dev The pointer to the device context.
*
* @return 1 if the DAC configuration is valid, 0 otherwise
*/
int ligo28ao32_is_dac_config_valid(ligo28ao32_dev_t * dev);

/**
* @brief Reads the samp_status register and returns the value
*        of the bit describing if the DAC DMA is running.
*
* @param dev The pointer to the device context.
*
* @return 1 if the DAC DMA is running, 0 otherwise
*/
int ligo28ao32_is_dac_dma_running(ligo28ao32_dev_t * dev);


/**
* @brief Queues the samples samples passed into the next DAC DMA buffer
*        location.
*
* @prerequisite The ADC DMAs MUSt be running so this function cal calculate 
*               what DMA buffer it should fill next.
*
* @param dev The pointer to the device context.
* @param num_samples The number of samples to queue from num_samples.
* @param samples An array of samples to queue.
*
* @return LIGO32AO32_OK If the samples were queued for TX.
*         LIGO32AO32_DAC_ERROR If the ADC is not running or num_samples 
*         is too high or samples does not point to valid memory.
*/
ligo28ao32_error_t ligo28ao32_queue_next_dac_dma(ligo28ao32_dev_t * dev, uint32_t num_samples, uint32_t samples []);

/**
* @brief This function retrieves a pointer to the DAC DMA buffer that will 
*        be TX'ed at the tiem requested. The GPS time and cycle information
*        is combined to get the correct DMA buffer.
*
* The first time this function is called it will enable the DAC DMAs automatically.
* The DMA and DAC TX will then fire according to the locked time and the previous 
* configuration passed by the ligo28ao32_dma_config() function.
*
* @param dev The pointer to the device context.
* @param gps_time_s The GPS time of the buffer to retrieve (in seconds).
* @param cur_cycle_64kclk The cycle of the buffer to retrieve.
*
* @return The pointer to the requested DMA buffer.
*/
void * ligo28ao32_get_next_dac_dma_queue(ligo28ao32_dev_t * dev, uint32_t gps_time_s, uint32_t cur_cycle_64kclk);



//
//DMA Functions
void ligo28ao32_dma_config(ligo28ao32_dev_t * dev, unsigned sample_rate_hz, unsigned dma_rate_hz,
                           unsigned adc_sample_delay, unsigned adc_dma_delay,
                           unsigned dac_sample_delay, unsigned dma_dma_delay);

/**
* @brief Enables the ADC DMA engine. After this is done it will fire the DMA in 
*        accordance with the configuration passed with ligo28ao32_dma_config()
*
* @param dev The pointer to the device context.
*
*/
void ligo28ao32_dma_start_adcs(ligo28ao32_dev_t * dev);

/**
* @brief Immediately stops the ADC DMA engine. 
*
* @param dev The pointer to the device context.
*
*/
void ligo28ao32_dma_stop_adcs(ligo28ao32_dev_t * dev);

/**
* @brief Enables the DAC DMA engine. After this is done it will fire the DMA in 
*        accordance with the configuration passed with ligo28ao32_dma_config()
*
* @param dev The pointer to the device context.
*
*/
void ligo28ao32_dma_start_dacs(ligo28ao32_dev_t * dev);

/**
* @brief Immediately stops the DAC DMA engine. 
*
* @param dev The pointer to the device context.
*
*/
void ligo28ao32_dma_stop_dacs(ligo28ao32_dev_t * dev);

// DMA Error Utils
int ligo28ao32_dma_query_adc_error_ctr(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_clear_adc_error_ctr(ligo28ao32_dev_t * dev);
int ligo28ao32_dma_query_dac_error_ctr(ligo28ao32_dev_t * dev);
void ligo28ao32_dma_clear_dac_error_ctr(ligo28ao32_dev_t * dev);
uint32_t ligo28ao32_dma_read_common_errors(ligo28ao32_dev_t * dev);
int ligo28ao32_dma_query_num_buff_per_adc_chan(ligo28ao32_dev_t * dev);


//Util Functions

void ligo28ao32_reset_watchdog(ligo28ao32_dev_t * dev);
uint32_t ligo28ao32_get_watchdog(ligo28ao32_dev_t * dev);

int ligo28ao32_samp_stat_is_timing_ok(uint32_t samp_status_reg);
int ligo28ao32_get_temp_c(ligo28ao32_dev_t * dev);

void ligo28ao32_print_status_registers(ligo28ao32_dev_t * dev);
void ligo28ao32_print_dma_and_converter_status(ligo28ao32_dev_t * dev);
void ligo28ao32_print_sampling_config(ligo28ao32_dev_t * dev);
void ligo28ao32_print_sampling_setup(ligo28ao32_dev_t * dev);
void ligo28ao32_print_converter_config(ligo28ao32_dev_t * dev);
void ligo28ao32_print_adc_dma_settings(ligo28ao32_dev_t * dev);

void ligo28ao32_dump_ligo_regs(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t num_regs);
bool ligo28ao32_read_reg(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t * value);
bool ligo28ao32_write_reg(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t value);

void ligo28ao32_print_dma_buffer_status(ligo28ao32_dev_t * dev, uint32_t status);
void ligo28ao32_print_samp_status_reg( uint32_t samp_status );

#endif //LIGO_28AO32_H
