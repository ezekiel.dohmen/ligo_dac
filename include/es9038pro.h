#ifndef LIGO_ES9038PRO_H
#define LIGO_ES9038PRO_H

#include <linux/types.h>

//SABRE® ES9038PRO Flagship HyperStream® II
//Register memory map
typedef struct es9038pro_regs_t
{
    uint8_t SYSTEM_REGISTERS;
    uint8_t INPUT_SELECTION;
    uint8_t SER_DATA_CONFIG_AND_AUTO_ENABLE;
    uint8_t RESERVED0;
    uint8_t AUTOMUTE_TIME;
    uint8_t AUTOMUTE_LEVEL;
    uint8_t DE_EMPTH_FILTER_AND_VOL_RAMP_RATE;
    uint8_t FILT_BW_AND_SYS_MUTE;
    uint8_t GPIO_1_2_CONFIG;
    uint8_t GPIO_3_4_CONFIG;
    uint8_t MASTER_MODE_AND_SYNC_CONFIG;
    uint8_t SPDIF_MUX_AND_GPIO_INVERSION;
    uint8_t JITTER_ELIM_AND_DPLL_BW;
    uint8_t JITTER_ELIM_AND_DPLL_CONFIG_AND_THD_BYPASS;
    uint8_t SOFT_START_SONFIG;
    uint8_t GPIO_INPUT_SEL_AND_VOL_CONTROL;
    //There are more regs, if needed.

} es9038pro_regs_t;


#endif //LIGO_ES9038PRO_H