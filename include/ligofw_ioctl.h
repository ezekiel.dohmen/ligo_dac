#ifndef LIGOFW_IOCTL_H
#define LIGOFW_IOCTL_H

typedef enum ligofw_ioctl_t
{
    LIGOFW_WRITE_REGS,
    LIGOFW_READ_REGS

} ligofw_ioctl_t;

typedef struct ligo_ioctl_write_regs_t {
    uint32_t addr_offset;
    uint32_t num_regs;
    uint32_t * vals;
} ligo_ioctl_write_regs_t;

typedef struct ligo_ioctl_read_regs_t {
    uint32_t addr_offset;
    uint32_t num_regs;
    uint32_t * vals;
} ligo_ioctl_read_regs_t;

union ligo_any_cmd_t {
    ligo_ioctl_write_regs_t write_cmd;
    ligo_ioctl_read_regs_t read_cmd;
};

#endif //LIGOFW_IOCTL_H