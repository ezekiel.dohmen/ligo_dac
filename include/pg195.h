#ifndef XILINX_PG195_H
#define XILINX_PG195_H

typedef struct dma_channel_regs_t {
        uint32_t chan_info;    //0x0
        uint32_t control_rw;  //0x4
        uint32_t control_w1s; //0x8
        uint32_t control_w1c; //0x0C
        uint32_t start_dma;   //0x10
        uint32_t unkown_3;    //0x14
        uint32_t stop_dma;    //0x18
        uint8_t pad0[0xE4];   //Pad out to 0x100
} dma_channel_regs_t;


typedef struct pg195_dma_ctrl_regs_t {


    dma_channel_regs_t host_to_card_chan_0;
    dma_channel_regs_t host_to_card_chan_1;
    uint32_t pad0[0x380]; //Pad out to 0x1000
    dma_channel_regs_t card_to_host_chan_0;
    dma_channel_regs_t card_to_host_chan_1;


} pg195_dma_ctrl_regs_t;


#endif //XILINX_PG195_H