#ifndef LIGO_PTC_H
#define LIGO_PTC_H

#include <linux/types.h>

struct ligoPTC_dev_t; 

int ligoPTC_is_locked(struct ligoPTC_dev_t * dev);
uint64_t ligoPTC_get_time_ns(struct ligoPTC_dev_t * dev);
uint32_t ligoPTC_get_fw_version(struct ligoPTC_dev_t * dev);
int ligoPTC_set_slot_config(struct ligoPTC_dev_t * dev, int index, int config);
void ligoPTC_set_bp_config(struct ligoPTC_dev_t * dev, int config);
bool ligoPTC_read_reg(struct ligoPTC_dev_t * dev, uint32_t offset, uint32_t * value);
bool ligoPTC_write_reg(struct ligoPTC_dev_t * dev, uint32_t offset, uint32_t value);

void ligoPTC_print_card_config(struct ligoPTC_dev_t * dev);


struct ligoPTC_dev_t * ligoPTC_configureDevice(unsigned instance);
void ligoPTC_freeDevice(struct ligoPTC_dev_t * dev);


#define LPTC_SCR_TIM_SIG               0x2000     // Bit 13: Use timing signal as clock
#define LPTC_SCR_LVDS               0x10000     // Bit 16: Use LVDS clock lines

#define LPTC_SCR_CLK_ENABLE		    0x100       // Bit 8 : Enable clock signal
#define LPTC_SCR_CLK_INVERT		    0x200       // Bit 9 : Inverted clock signal
#define LPTC_SCR_CLK_1PPS           0x400       // Bit 10: Start clock at next second boundary
#define LPTC_SCR_IDLE_TRANS         0x800       // Bit 11: Start clock at next transition from idle
#define LPTC_SCR_IDLE_HIGH		    0x1000      // Bit 12: Full Idle clock line high
#define LPTC_SCR_TIM_SIG            0x2000      // Bit 13: Use timing signal as clock
//                                                 Bit 14-15: Reserved
#define LPTC_SCR_LVDS               0x10000     // Bit 16: Use LVDS clock lines
#define LPTC_SCR_ADC_DT_ENABLE		0x20000     // Bit 17: Enable ADC Duotone on last ADC chan
#define LPTC_SCR_DAC_DT_ENABLE		0x40000     // Bit 18: Enable DAC Duotone on last -1  ADC chan

#define LPTC_SCR_ADC_SET		    (LPTC_SCR_CLK_ENABLE | LPTC_SCR_CLK_INVERT | LPTC_SCR_IDLE_HIGH)
#define LPTC_SCR_DAC_SET		    (LPTC_SCR_CLK_ENABLE | LPTC_SCR_IDLE_HIGH)

#define LPTC_BP_SLOTS 10
#define LPTC_OBF_EPS_REGS 8


typedef struct SLOT_CONFIG {
    u32 config;
    u32 phase;
    u32 status;
    u32 reserved;
}SLOT_CONFIG;


typedef struct LPTC_REGISTER {
    u64 gps_time;       // 0x0000
    u32 status;         // 0x0008
    u32 revision;       // 0x000c
    u32 bp_config;      // 0x0010
    u32 wd_reset;       // 0x0014
    u32 bp_status;      // 0x0018
    u32 reserved3;      // 0x001c
    SLOT_CONFIG slot_info[LPTC_BP_SLOTS];   // 0x0020
}LPTC_REGISTER;

typedef struct LPTC_OBF_REGISTER {
    u32 brd_config; // 0x0180
    u32 xadc_config;    // 0x0184
    u32 bps_status; // 0x0188
    u32 xadc_status;    // 0x018c
    u32 tips1;      // 0x0190
    u32 ips2;       // 0x0194
    u32 eps[LPTC_OBF_EPS_REGS];     // 0x0198
}LPTC_OBF_REGISTER;


// Data read from LPTC at 0x1000 offset
typedef struct LPTC_DIAG_INFO {
    u32 board_id;       // 0x1000
    u32 board_sn;       // 0x1004
    u32 sftware_id;     // 0x1008
    u32 sftware_rev;    // 0x100c
    u32 gps_seconds;    // 0x1010
    u32 mod_address;    // 0x1014
    u32 board_status;   // 0x1018
    u32 board_config;   // 0x101c
    u32 ocxo_controls;      // 0x1020
    u32 ocxo_error;         // 0x1024
    u32 uplink_1pps_delay;  // 0x1028
    u32 external_1pps_delay; //0x102c
    u32 gps_1pps_delay;     //0x1030
    u32 fanout_up_loss;     //0x1034
    u32 fanout_missing_delay_error; //0x1038
    u32 leaps_and_error;    //0x103c
}LPTC_DIAG_INFO;

//complete lptc register space
typedef struct LPTC_REGISTER_SPACE {
    LPTC_REGISTER lptc_registers;   //ends at 0xc0
    u64 unused[0x18];   //another 0xc0 bytes gets to offset 0x180
    LPTC_OBF_REGISTER obf_registers; //starts at 0x180, ends at 0x1a4
    u32 unused2[0x392];  //another 0xe48 bytes gets to 0x1000.
    LPTC_DIAG_INFO diag_info;
} LPTC_REGISTER_SPACE;



#endif //LIGO_PTC_H