#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/stddef.h>

//rts-cpu-isolator
#include "rts-cpu-isolator.h"

//rts-logger
#define RTS_LOG_PREFIX "ld32"
#include "drv/rts-logger.h"



#include "hist.h"
#include "ligo28ao32.h"
#include "ligo28ao32_private.h"
#include "ligo_fw_utils.h"
#include "ligoPTC.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("LIGO");
MODULE_DESCRIPTION("Driver for the LIGO DAC PCIe card");

static atomic_t g_atom_should_exit = ATOMIC_INIT(0);
static atomic_t g_atom_has_exited = ATOMIC_INIT(0);
static int g_core_used;

#define MAX_SUPPORTED_DACS 4

static hist_context_t * g_adc_timing_hists[MAX_SUPPORTED_DACS];

struct ligo28ao32_dev_t * dac_devs[MAX_SUPPORTED_DACS] = {NULL,};
struct ligoPTC_dev_t * timing_dev = NULL;

//
// Start Module Parameters
//

static int DAC_PCIE_INSTANCES_ARR[MAX_SUPPORTED_DACS];
static int g_num_dacs;
module_param_array(DAC_PCIE_INSTANCES_ARR, int, &g_num_dacs, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(DAC_PCIE_INSTANCES_ARR, "The instance numbers of the dacs you would like to bring up");

static int DAC_IO_SLOTS_ARR[MAX_SUPPORTED_DACS];
static int g_num_ioc_slots;
module_param_array(DAC_IO_SLOTS_ARR, int, &g_num_ioc_slots, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(DAC_IO_SLOTS_ARR, "The IO chassis slot numbers corresponding to the DAC array parameter");

static int INSTANCES_TO_RUN_ADC_DMA_ARR[MAX_SUPPORTED_DACS];
static int g_num_adc_dmas;
module_param_array(INSTANCES_TO_RUN_ADC_DMA_ARR, int, &g_num_adc_dmas, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(INSTANCES_TO_RUN_ADC_DMA_ARR, "The instnace numbers of the cards to run ADC DMA on");
static int g_run_adc_dma[MAX_SUPPORTED_DACS] = {0};

static int INSTANCES_TO_RUN_DAC_DMA_ARR[MAX_SUPPORTED_DACS];
static int g_num_dac_dmas;
module_param_array(INSTANCES_TO_RUN_DAC_DMA_ARR, int, &g_num_dac_dmas, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(INSTANCES_TO_RUN_DAC_DMA_ARR, "The instnace numbers of the cards to run DAC DMA on");
static int g_run_dac_dma[MAX_SUPPORTED_DACS] = {0};


// Debugging module parameters
static int DUMP_LAST_ADC_BUF_STATUS = 0;
module_param(DUMP_LAST_ADC_BUF_STATUS, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);

static int DUMP_COMMON_ERRORS_REGISTER = 0;
module_param(DUMP_COMMON_ERRORS_REGISTER, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);


void rt_func( void )
{
    uint64_t gps_ns, num_good_adc=0;
    uint64_t time_tsc = 0, total_time_ns=0;
    uint32_t adc_buf_status[MAX_SUPPORTED_DACS];
    uint32_t dac_error_counters[MAX_SUPPORTED_DACS] = {0,};
    uint32_t dac_error_counters_per_sec[MAX_SUPPORTED_DACS] = {0,};
    uint32_t dac_good_tx_counters[MAX_SUPPORTED_DACS] = {0,};
    uint32_t last_dac_error_adc_count[MAX_SUPPORTED_DACS] = {0,};
    uint32_t dac_last_error[MAX_SUPPORTED_DACS] = {0,};
    ligo28ao32_error_t dac_ret;


    for (int dac=0; dac<g_num_dacs; ++dac) {
        if( g_run_adc_dma[dac] )
            RTSLOG_INFO("Going to run ADC DMA on card index %d\n", dac);
        if( g_run_dac_dma[dac] )
            RTSLOG_INFO("Going to run DAC DMA on card index %d\n", dac);

        //ligo28ao32_dma_stop_dacs(dac_devs[dac]);
        //ligo28ao32_print_sampling_setup(dac_devs[dac]);
        //ligo28ao32_print_sampling_config(dac_devs[dac]);
    }


    //Wait for stable timing
    while(num_good_adc < 300) {
        udelay(10000);
        ++num_good_adc;
    }
    num_good_adc = 0;

    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        ligo28ao32_dma_clear_adc_error_ctr(dac_devs[dac]);
        ligo28ao32_dma_clear_dac_error_ctr(dac_devs[dac]);
        ligo28ao32_dma_read_common_errors(dac_devs[dac]); //Clear errors, for read on next line
        //RTSLOG_INFO("ligo28ao32_dma_read_common_errors(dac_devs[dac]): %u\n", ligo28ao32_dma_read_common_errors(dac_devs[dac]));
    }

    unsigned dac_sample_and_dma_delay = 32767;
    for (int dac=0; dac<g_num_dacs; ++dac)
    {

        //ligo28ao32_print_sampling_config(dac_devs[dac]);
        ligo28ao32_dma_config(dac_devs[dac], 
                              65536, //ADC/DAC Sample rate Hz
                              65536, //ADC/DAC Dma rate Hz
                              0xFFFFFFFF,//ADC sample delay
                              0x3FF, //ADC DMA delay
                              32767, //DAC sample delay 
                              32767 //Dac DMA delay
                             );

        dac_sample_and_dma_delay += 500;
        //ligo28ao32_print_converter_config(dac_devs[dac]); 
        //ligo28ao32_print_sampling_setup(dac_devs[dac]);

        if ( !ligo28ao32_is_adc_config_valid(dac_devs[dac]) || !ligo28ao32_is_locked(dac_devs[dac])) { //Valid config, start DMA
            RTSLOG_INFO("CARD: %d, ADC config not valid (%u), or DAC timing unlocked (is_locked: %u). Exiting.\n",
            dac, ligo28ao32_is_adc_config_valid(dac_devs[dac]), ligo28ao32_is_locked(dac_devs[dac]));
            goto isolated_exit;
        }

        //if ( ! ligo28ao32_is_dac_config_valid(dac_devs[dac]) ){
        //    RTSLOG_ERROR("DAC %d, card says configuration is bad.\n", dac);
        //    goto isolated_exit;
       //}
    }

    //Start the ADC DMAs
    udelay(19999);
    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        if( g_run_adc_dma[dac] )
            ligo28ao32_dma_start_adcs(dac_devs[dac]);
    }
    udelay(19999);
    
    timer_start( &total_time_ns );

    //Mark ADC data as 'read'
    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        if( g_run_adc_dma[dac] )
            ligo28ao32_consume_adc_buffer(dac_devs[dac]);
    }


    while( atomic_read(&g_atom_should_exit) == 0 ) {



        for (int dac=0; dac<g_num_dacs; ++dac)
        {



            if( g_run_adc_dma[dac] ) {

                timer_start( &time_tsc );

                dac_ret = ligo28ao32_wait_adc_dma(dac_devs[dac], 20, &gps_ns);
                if(dac_ret != LIGO32AO32_OK) {
                    RTSLOG_ERROR("Timeout waiting for ADC(%d) DMA transfer. num_good_adc: %llu\n", dac, num_good_adc);
                    goto isolated_exit;
                }

                hist_add_element(g_adc_timing_hists[dac], timer_end_ns( &time_tsc ));
                if( time_tsc > 25000)
                {
                    RTSLOG_ERROR("Got a long wait in cpu time: %lu ns, cur sample cnt: %lu\n", time_tsc, num_good_adc);
                }

                //Now that the ADC dma is done, we can read status from the DMAed ADC buffer
                adc_buf_status[dac] = ligo28ao32_get_adc_buf_status(dac_devs[dac]);
                if( DUMP_LAST_ADC_BUF_STATUS ) {
                    RTSLOG_INFO("\nCard Index %d\n", dac);
                    ligo28ao32_print_dma_buffer_status(NULL, adc_buf_status[dac]);
                    RTSLOG_RAW("\n");
                    if( dac == g_num_dacs - 1) //Clear on last DAC
                        DUMP_LAST_ADC_BUF_STATUS = 0;
                }


            }

            if( DUMP_COMMON_ERRORS_REGISTER ) {
                RTSLOG_INFO("\nCard Index %d\n", dac);
                ligo28ao32_print_samp_status_reg(ligo28ao32_dma_read_common_errors(dac_devs[dac]));
                RTSLOG_RAW("\n");
                if( dac == g_num_dacs - 1) //Clear on last DAC
                    DUMP_COMMON_ERRORS_REGISTER = 0;
            }


        } //End for over each dac
        ++num_good_adc;


        //Simulated cycle time
        //udelay(14); //14 us shifts the ADC wait to 


        //Mark ADC data as 'read'
        for (int dac=0; dac<g_num_dacs; ++dac)
        {
            ligo28ao32_consume_adc_buffer(dac_devs[dac]);

            if( g_run_dac_dma[dac] ) {

                //Mark next DAC DMA timestamp good

                //Generate next DAC timestamp from last ADC timestamp
                uint32_t cur_dac_tick = convert_ns_to_4GHz(GET_NANO_FROM_GPS_NS(gps_ns));
                if( (cur_dac_tick & 0x0000FFFF) > 32768) cur_dac_tick = (cur_dac_tick + 65536); //?
                cur_dac_tick &= 0xFFFF0000;
                cur_dac_tick = cur_dac_tick >> 16; //< To 65k clk domain


                ligo28ao32_get_next_dac_dma_queue(dac_devs[dac], GET_SEC_FROM_GPS_NS(gps_ns), cur_dac_tick);



                //Check DAC status in ADC DMA buffer

                if ((adc_buf_status[dac] & 0x3C000000) != 0 ) {
                    ++dac_error_counters[dac];
                    ++dac_error_counters_per_sec[dac];
                    last_dac_error_adc_count[dac] = num_good_adc;

                    //Read errors register from card, and clear
                    dac_last_error[dac] = ligo28ao32_dma_read_common_errors(dac_devs[dac]);
                    //if ((dac_last_error[dac] & 0x3C000000) != 0 ) {
                    //    RTSLOG_ERROR("DAC %d reported error (0x%x) in status register\n", dac, dac_last_error[dac]);
                    //}
                 

                }
                else if ( g_run_dac_dma[dac] ){
                    ++dac_good_tx_counters[dac];
                }

                //Also make sure the DACs/DAC DMA/Valid bit are all good (in ADC buff, if running adc)
                if ( g_run_adc_dma[dac] && 
                     (GET_BIT(adc_buf_status[dac], DMA_STAT_DACS_RUNNING) != 1 ||
                      GET_BIT(adc_buf_status[dac], DMA_STAT_DAC_DMA_RUNNING) != 1 ||
                      GET_BIT(adc_buf_status[dac], DMA_STAT_DAC_DATA_VALID) != 1 )) {
                    //TODO: This check does not seem to work as is
                    //RTSLOG_WARN("DAC %d status in ADC buffer says DAC may not be running 0x%x\n", dac, adc_buf_status[dac]);

                     }

                


            } //if( g_run_dac_dma[dac] )




            if ( g_run_adc_dma[dac] && (adc_buf_status[dac] & 0xF00) != 0 )
            {
                RTSLOG_ERROR("ADC %d Error detected! Exiting...\n", dac);
                //break;
            }

            if ( num_good_adc % 65536 == 0) {
                if( dac_error_counters_per_sec[dac] != 0)
                    RTSLOG_WARN("DAC %d had %d errors over the last second.\n", dac, dac_error_counters_per_sec[dac]);
                dac_error_counters_per_sec[dac] = 0;
            }

        }





    } //while( atomic_read(&g_atom_should_exit) == 0 ) 

    timer_end_ns( &total_time_ns );

    //RTSLOG_INFO("Before DAC stop: \n");
    //ligo28ao32_print_dma_buffer_status(dac_dev, ligo28ao32_get_locked_status(dac_dev));
    //ligo28ao32_dma_stop_dacs(dac_dev);
    udelay(19999); 
    udelay(19999); 
    RTSLOG_INFO("DACs have been stopped...\n");




    RTSLOG_INFO("num_good_adc: %llu\n", num_good_adc);

    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        RTSLOG_INFO(" **** DAC %d ****\n dac_good_tx_counters: %d, dac_error_counters: %u, enabled?: %d\n", 
                    dac, dac_good_tx_counters[dac], dac_error_counters[dac], g_run_dac_dma[dac]);
        if(dac_error_counters[dac] > 0) {
            RTSLOG_INFO("BAD DAC %d TX detected: last_dac_error_adc_count: %lu\n "
                        "   Last Bad Status REG: NOT_READY_0: %u, NOT_READY_1: %u, NO_DATA_0: %u, NO_DATA_1: %u, TIME_ER_0: %u, TIME_ER_1: %u\n\n", 
                dac, last_dac_error_adc_count[dac], 
                GET_BIT(dac_last_error[dac], DMA_STAT_DAC_DMA_CHAN_0_NOT_READY), 
                GET_BIT(dac_last_error[dac], DMA_STAT_DAC_DMA_CHAN_1_NOT_READY), 
                GET_BIT(dac_last_error[dac], DMA_STAT_DAC_DMA_CHAN_0_NO_DATA), 
                GET_BIT(dac_last_error[dac], DMA_STAT_DAC_DMA_CHAN_1_NO_DATA),
                GET_BIT(dac_last_error[dac], DMA_STAT_DAC_DMA_CHAN_0_TIME_ERR),
                GET_BIT(dac_last_error[dac], DMA_STAT_DAC_DMA_CHAN_1_TIME_ERR)
                );
        }
    }

    //RTSLOG_INFO("Num good DAC tx: %llu, num BAD DAC: %llu, last DAC error in DMA status: %llu, last_good_dac: %lld\n", 
    //       num_good_adc_dac_txs, num_good_adc-num_good_adc_dac_txs, last_dac_error, last_good_dac);
    //RTSLOG_INFO("Calculated avg DAC error rate : %u/s\n", (num_good_adc-num_good_adc_dac_txs)/(total_time_ns/1000000000));
    //RTSLOG_INFO("Minimum frac time recorded was: %u ns \n", min_frac_ns);
    //RTSLOG_INFO(KERN_INFO "Last ligo28ao32_get_locked_status(): %u\n", ligo28ao32_get_locked_status(dac_dev));
    //ligo28ao32_print_dma_buffer_status(dac_dev, ligo28ao32_get_locked_status(dac_dev));

    isolated_exit:
    while(atomic_read(&g_atom_should_exit) == 0 ) {};
    atomic_set(&g_atom_has_exited, 1);
    return;
}


static int __init ld32_init(void) {
 
    int64_t ranges [] = {100, 200, 300, 400, 500, 600, 700, 800, 1200, 2000, 
                        12000, 13000, 14000, 14500, 15000, 15500, 16000, 16500, 17000, 18000, 20000};
    


    if( g_num_dacs != g_num_ioc_slots ) {
        RTSLOG_ERROR("The number of DACs (%d) passed in did not match the number of IOC slots(%d) for those DACs\n",
                     g_num_dacs, g_num_ioc_slots);
        return -1;
    }

    if ( g_num_dacs > MAX_SUPPORTED_DACS ) {
        RTSLOG_ERROR("The number of DACs (%d) passed in, is too many for this modules to support (%d)\n",
                     g_num_dacs, MAX_SUPPORTED_DACS);
        return -1;
    }

    if( g_num_adc_dmas > MAX_SUPPORTED_DACS ) {
        RTSLOG_ERROR("The number of instances in the INSTANCES_TO_RUN_ADC_DMA_ARR parameter (%d), is too many for this modules to support (%d)\n",
                     g_num_adc_dmas, MAX_SUPPORTED_DACS);
        return -1;
    }

    if( g_num_dac_dmas > MAX_SUPPORTED_DACS ) {
        RTSLOG_ERROR("The number of instances in the INSTANCES_TO_RUN_DAC_DMA_ARR parameter (%d), is too many for this modules to support (%d)\n",
                     g_num_dac_dmas, MAX_SUPPORTED_DACS);
        return -1;
    }

    //
    //Figure out what cards to run ADC DMAs on
    if( g_num_adc_dmas == 0 ) {
        RTSLOG_INFO("Module parameter INSTANCES_TO_RUN_ADC_DMA_ARR was not passed, so defaulting to running ADC DMA on all crads.\n");
        //for(int inst=0; inst<g_num_dacs; ++inst) 
            //g_run_adc_dma[inst] = 1;
    }
    else {

        for(int dac=0; dac<g_num_adc_dmas; ++dac ) {
            unsigned found = 0;
            unsigned cur_instance = INSTANCES_TO_RUN_ADC_DMA_ARR[dac];
            if( cur_instance >= MAX_SUPPORTED_DACS) {
                RTSLOG_WARN("Instance %u in INSTANCES_TO_RUN_ADC_DMA_ARR too high, skiping\n");
                continue;
            }

            for(int inst=0; inst<g_num_dacs; ++inst) {
                if( cur_instance == DAC_PCIE_INSTANCES_ARR[inst]) {
                    g_run_adc_dma[DAC_PCIE_INSTANCES_ARR[inst]] = 1;
                    found =1;
                }
            }
            if( !found ) {
                RTSLOG_WARN("Instance num %d was not found in the DAC_PCIE_INSTANCES_ARR, skipping\n");
            }

        }
    }

    //
    //Figure out what cards to run DAC dmas on
    if( g_num_dac_dmas == 0 ) {
        RTSLOG_INFO("Module parameter INSTANCES_TO_RUN_DAC_DMA_ARR was not passed, so defaulting to running DAC DMA on all crads.\n");
        //for(int inst=0; inst<g_num_dacs; ++inst) 
            //g_run_dac_dma[inst] = 1;
    }
    else {

        for(int dac=0; dac<g_num_dac_dmas; ++dac ) {
            unsigned found = 0;
            unsigned cur_instance = INSTANCES_TO_RUN_DAC_DMA_ARR[dac];
            if( cur_instance >= MAX_SUPPORTED_DACS) {
                RTSLOG_WARN("Instance %u in INSTANCES_TO_RUN_DAC_DMA_ARR too high, skiping\n");
                continue;
            }

            for(int inst=0; inst<g_num_dacs; ++inst) {
                if( cur_instance == DAC_PCIE_INSTANCES_ARR[inst]) {
                    g_run_dac_dma[DAC_PCIE_INSTANCES_ARR[inst]] = 1;
                    found =1;
                }
            }
            if( !found ) {
                RTSLOG_WARN("Instance num %d was not found in the DAC_PCIE_INSTANCES_ARR, skipping\n");
            }

        }
    }



    for(int dac=0; dac<g_num_dacs; ++dac ){
        g_adc_timing_hists[dac] = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
        if( g_adc_timing_hists[dac] == NULL) {
            RTSLOG_ERROR("Failed to allocate hist, exiting...\n");
            return -1;
        }

    }

    //Find and init timing card
    RTSLOG_INFO("Initializing timing card...\n");
    timing_dev = ligoPTC_configureDevice( 0 );
    if ( !timing_dev ) {
        RTSLOG_INFO("ligoPTC_configureDevice() failed, exiting.");
        return -1;
    }
    if( !ligoPTC_is_locked(timing_dev) ) {
        RTSLOG_ERROR("Timing Card is_locked status : %u\n", ligoPTC_is_locked(timing_dev));
        goto cleanup_cards;
    }



    //Find and init DACs
    RTSLOG_INFO("Starting to configure %d DAC cards...\n", g_num_dacs);
    for (int dac=0; dac<g_num_dacs; ++dac) 
    {
        dac_devs[dac] = ligo28ao32_configureDevice( DAC_PCIE_INSTANCES_ARR[dac] );
        if ( !dac_devs[dac] ) {
            RTSLOG_INFO("ligo28ao32_configureDevice() failed, DAC %d Instance %d, exiting.\n", 
                        dac, DAC_PCIE_INSTANCES_ARR[dac]);
            goto cleanup_cards;
        }
        RTSLOG_INFO("Reported converter slot : 0x%x\n", ligo28ao32_get_backplane_slot(dac_devs[dac]) );
    }



    //Configure the timing card to send clk signal to the DAC boards
    int count = 0;
    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        ligoPTC_set_slot_config(timing_dev, DAC_IO_SLOTS_ARR[dac], LPTC_SCR_TIM_SIG | LPTC_SCR_LVDS);
        while (ligo28ao32_is_locked(dac_devs[dac]) == 0 && count++ < 100) {
            msleep(100);
        }

        if( ligo28ao32_is_locked(dac_devs[dac]) == 0) {
            RTSLOG_ERROR("DAC instance %d did not lock ni the time alloted, exiiting\n", DAC_PCIE_INSTANCES_ARR[dac]);
            goto cleanup_cards;
        }
    }


    //Do a bit more timing card backplane configuration
    ligoPTC_set_bp_config(timing_dev, 0);//Clear out config from advligo
    //ligoPTC_print_card_config(timing_dev);
    RTSLOG_INFO("ligoPTC_get_fw_version: 0x%x\n", ligoPTC_get_fw_version(timing_dev));



    //Start isolated control loop
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    RTSLOG_INFO("ligo28ao32 driver : Locking free CPU core\n" );

    ret = rts_isolator_run( rt_func, -1, &g_core_used);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : %s\n", error_msg);
        return -1;
    }


    RTSLOG_INFO("ligo28ao32 insert done.\n");
    return 0; //END of nominal case


cleanup_cards:
    for (int dac=0; dac<g_num_dacs; ++dac)
    {

        if( dac_devs[dac] ) {
            ligo28ao32_freeDevice( dac_devs[dac] );
        }
    }

    if( timing_dev ) {
        ligoPTC_freeDevice(timing_dev);
    }
    return -1;

}


static void __exit ld32_exit(void) {

    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    uint64_t stop_sig_time_ns = get_monotonic_time_ns();

    //ligo28ao32_print_sampling_config(dac_dev);

    // Stop the code and wait
    atomic_set(&g_atom_should_exit, 1);
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    RTSLOG_INFO("It took %lld ms for the RT code to exit.\n",
                 (get_monotonic_time_ns() - stop_sig_time_ns)/1000000);

    //At this point the CPU will be down...
    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }


    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        ligo28ao32_dma_stop_adcs(dac_devs[dac]);
        ligo28ao32_dma_stop_dacs(dac_devs[dac]);
    }
    msleep(100); //Wait for any started transfer to complete


    //Free up DACs and timing card
    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        if( dac_devs[dac] ) 
            ligo28ao32_freeDevice( dac_devs[dac] );
    }

    if( timing_dev ) {
        ligoPTC_freeDevice(timing_dev);
    }






    for (int dac=0; dac<g_num_dacs; ++dac)
    {
        RTSLOG_INFO("Histogram Of ADC(%d) Wait Times by CPU Time (ns)\n", dac);
        msleep(100);
        hist_print_stats(g_adc_timing_hists[dac]);
        hist_free(g_adc_timing_hists[dac]);
    }


	RTSLOG_INFO("Unloaded ld32 driver!\n");

}

module_init(ld32_init);
module_exit(ld32_exit);
