#include "LIGO_Dac.hh"

#include <iostream>

int main()
{
    uint32_t regs[10];
    //Attempt to open device 0, ie. /dev/ligo28ao32-0
    std::unique_ptr<LIGO::LIGO_Dac> dac = LIGO::LIGO_Dac::create_instance( 0 );
    if ( dac == nullptr ) return 0;

    //Read the time registers a few times
    for (int j=0; j<5; ++j ) {
        dac->readRegisters(0x0, 2, regs);
        std::cout << "Time (sec:frac) " << regs[1] << " : " << regs[0] << std::endl;
    }

    //Read the temp register
    dac->readRegisters(0x190, 1, regs);
    std::cout << "Temp : "<< (regs[0]&0xFFFF)* 503.975 / 65536.0 - 273.15 << " c\n";
    return 0;

}