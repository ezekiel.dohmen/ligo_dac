#include "LIGO_Dac.hh"

#include <iostream>

int main()
{
    uint32_t regs[10];
    std::unique_ptr<LIGO::LIGO_Dac> dac = LIGO::LIGO_Dac::create_instance( 0 );
    if ( dac == nullptr ) return -1;

    //Check and make sure this fails
    std::unique_ptr<LIGO::LIGO_Dac> dac2 = LIGO::LIGO_Dac::create_instance( 0 );
    if( dac2 != nullptr) return -1;

    //Write some data and read it back
    regs[0] = 0xDEADBEEF;
    regs[1] = 0xDEADDEAD;
    if ( !dac->writeRegisters(0xC0, 2, regs) ) return -1;
    regs[0] = 0x0;
    regs[1] = 0x0;
    if( !dac->readRegisters(0xC0, 2, regs) ) return -1;
    for ( int i=0; i < 2; ++i )
        std::cout << "reg " << std::dec << i << std::hex << " : " << regs[i] << std::endl;

    //Read temp register
    if( !dac->readRegisters(0x190, 1, regs) ) return -1;
    std::cout << "Temp reg : " << regs[0] << std::endl;
    std::cout << "Test Passed!\n";
    return 0;

}