#include "LIGO_PTC.hh"

#include <iostream>


int main()
{
    uint32_t regs[10];
    //Attempt to open device 0, ie. /dev/ligoPTC-0
    std::unique_ptr<LIGO::LIGO_PTC> dac = LIGO::LIGO_PTC::create_instance( 0 );
    if ( dac == nullptr ) return 0;


    dac->readRegisters(0x0, 2, regs);
    std::cout << "Time (sec:frac) " << regs[1] << " : " << regs[0] << std::endl;


    //Read the temp register
    dac->readRegisters(0x190, 1, regs);
    std::cout << "Temp : "<< (regs[0]&0xFFFF)* 503.975 / 65536.0 - 273.15 << " c\n";
    return 0;

}