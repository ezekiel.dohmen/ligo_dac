#ifndef LIGO_DAC_HH
#define LIGO_DAC_HH

#include "LIGO_FW_Base.hh"

#include <memory>


namespace LIGO 
{
    class LIGO_Dac : public LIGO_FW_Base
    {
        public:

       /**
        * @brief This static method must be used to create instances of this class
        *
        * Initializes and takes controls over the DAC instance requested. Returns 
        * nullptr if unsuccessful. Each DAC can only be initialized once, until the
        * class managing it is destructed. 
        *
        * @param instance_num Selects the DAC instance you would like to 
        *                     initialize/control. 0 will be the first DAC on the
        *                     bus, 1 the next and so on.
        *
        * @return The newly constructed DAC class or nullptr if there was 
        *         an nonrecoverable error.
        */
        static std::unique_ptr<LIGO_Dac> create_instance( int instance_num );

        virtual ~LIGO_Dac() {};



        private:

        explicit LIGO_Dac() {}; //Contructor is private, use create_instance( ... )

    };
}

#endif //LIGO_DAC_HH