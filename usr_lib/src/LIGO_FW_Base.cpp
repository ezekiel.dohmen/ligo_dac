#include "LIGO_FW_Base.hh"

#include "ligofw_ioctl.h"

#include <string>
#include <iostream>
#include <vector>

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>


using namespace LIGO;

bool LIGO_FW_Base::readRegisters(uint32_t addr_offset, uint32_t num_registers, uint32_t data_out[] )
{
    std::vector<uint32_t> data_store;
    data_store.resize(num_registers, 0); //Build output buffer

    ligo_ioctl_read_regs_t cmd = { .addr_offset = addr_offset,
                                   .num_regs = num_registers,
                                   .vals = &data_store[0]
                                 }; 

    if ( ioctl(_dev_fd, LIGOFW_READ_REGS, (int32_t*) &cmd) != 0 )
    {
        return false;
    }

    for( int i=0; i < num_registers; ++i )
        data_out[i] = data_store[i];

    return true;
}


bool LIGO_FW_Base::writeRegisters(uint32_t addr_offset, uint32_t num_registers, uint32_t data_in[] )
{
    ligo_ioctl_write_regs_t cmd = { .addr_offset = addr_offset,
                                    .num_regs = num_registers,
                                    .vals = data_in
                                  }; 

    if ( ioctl(_dev_fd, LIGOFW_WRITE_REGS, (int32_t*) &cmd) != 0 )
    {
        return false;
    }

    return true;
}

LIGO_FW_Base::~LIGO_FW_Base()
{
    if (_dev_fd >= 0)
        close( _dev_fd );
}



