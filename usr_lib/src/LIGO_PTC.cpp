#include "LIGO_PTC.hh"


#include <string>
#include <iostream>
#include <vector>

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>


using namespace LIGO;

std::unique_ptr<LIGO_PTC> LIGO_PTC::create_instance( int instance_num )
{
    std::unique_ptr<LIGO_PTC> me_ptr (new LIGO_PTC());

    std::string dev_path = "/dev/ligoPTC-" + std::to_string(instance_num);

    me_ptr->_dev_fd = open(dev_path.c_str(), O_RDWR);
    if( me_ptr->_dev_fd < 0 ) {
        std::cerr << __PRETTY_FUNCTION__ << " : Could not open the device at " << dev_path 
                  << ", instance_num " << instance_num  <<std::endl;
        perror("perror ");
        if ( errno == EBUSY ) std::cerr << "The device is already in use by another process.\n";
        if ( errno == ENOENT ) std::cerr << "Maybe the driver is not inserted?\n";
        if ( errno == ENXIO ) std::cerr << "There was an error when configuring the device.\n";
        return nullptr;
    }
    return me_ptr;
}

