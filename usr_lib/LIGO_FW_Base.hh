#ifndef LIGO_FW_BASE_HH
#define LIGO_FW_BASE_HH

#include <stdint.h>

namespace LIGO 
{

    class LIGO_FW_Base 
    {

        public:

        /**
            * @brief Reads and returns the register values from the requested offset.
            *
            * @param addr_offset The offset, within the DAC control registers to read from.
            * @param num_registers The number of registers to read.
            * @param data_out An array (at least num_registers long) for the read to store 
            *                 the register values read, starting from addr_offset.
            *
            * @return True if all reads was successful, false otherwise
            */
            bool readRegisters(uint32_t addr_offset, uint32_t num_registers, uint32_t data_out[] );

        /**
            * @brief Writes the register values to the requested offset.
            *
            * @param addr_offset The offset, within the DAC control registers to write to.
            * @param num_registers The number of registers to write.
            * @param data_out An array (at least num_registers long) filled with the values 
            *                 that should be written, starting at addr_offset.
            *
            * @return True if all writes were successful, false otherwise
            */
            bool writeRegisters(uint32_t addr_offset, uint32_t num_registers, uint32_t data_in[] );

            virtual ~LIGO_FW_Base();

        protected:
        explicit LIGO_FW_Base() {};
        int _dev_fd = -1;

        private:
    


    };

}

#endif //LIGO_FW_BASE_HH