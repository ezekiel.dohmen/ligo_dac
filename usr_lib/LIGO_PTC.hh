#ifndef LIGO_PTC_HH
#define LIGO_PTC_HH

#include "LIGO_FW_Base.hh"

#include <memory>


namespace LIGO 
{
    class LIGO_PTC : public LIGO_FW_Base
    {
        public:

       /**
        * @brief This static method must be used to create instances of this class
        *
        * Initializes and takes controls over the LIGO PCIe Timing Card instance requested. 
        * Returns nullptr if unsuccessful. Each timing card can only be initialized once, 
        * until the class managing it is destructed. 
        *
        * @param instance_num Selects the LPTC instance you would like to 
        *                     initialize/control. 0 will be the first LPTC on the
        *                     bus, 1 the next and so on.
        *
        * @return The newly constructed LPTC class or nullptr if there was 
        *         an nonrecoverable error.
        */
        static std::unique_ptr<LIGO_PTC> create_instance( int instance_num );

        virtual ~LIGO_PTC() {};



        private:

        explicit LIGO_PTC() {}; //Contructor is private, use create_instance( ... )

    };
}

#endif //LIGO_PTC_HH