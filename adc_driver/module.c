#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/stddef.h>

//rts-cpu-isolator
#include "rts-cpu-isolator.h"

//rts-logger
#define RTS_LOG_PREFIX "ld32"
#include "drv/rts-logger.h"



#include "hist.h"
#include "ligo28ao32.h"
#include "ligo28ao32_private.h"
#include "ligo_fw_utils.h"
#include "ligoPTC.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("LIGO");
MODULE_DESCRIPTION("Driver for the LIGO DAC PCIe card");

static atomic_t g_atom_should_exit = ATOMIC_INIT(0);
static atomic_t g_atom_has_exited = ATOMIC_INIT(0);
static int g_core_used;
static hist_context_t * g_hist_ctx_raw;
static hist_context_t * g_hist_ctx_cpu;

struct ligo28ao32_dev_t * dac_dev = NULL;
struct ligoPTC_dev_t * timing_dev = NULL;

//
// Start Module Parameters
//
static int IOC_SLOT = 0; //ADC is not in this IO slot, TODO I though slot 0 was always timing card?
module_param(IOC_SLOT, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(IOC_SLOT, "The IO slot index of the LIGO DAC");

static int DAC_PCIE_INSTANCE = 0;
module_param(DAC_PCIE_INSTANCE, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(DAC_PCIE_INSTANCE, "The IO slot index of the LIGO DAC");



#define NUM_SAMPLES_TO_RECORD 1000000
int32_t g_data_cap[NUM_SAMPLES_TO_RECORD];
void write_data_to_file( void ) 
{
    struct file *i_fp;
    loff_t pos = 0;
    ssize_t bytes_out;
    i_fp = filp_open("/home/controls/ej/ligo_dac/adc_driver/output.int32",O_RDWR | O_CREAT, 0666);
    if (IS_ERR(i_fp)) {
        printk(KERN_INFO "intput file open error/n");
        return;
    }

    bytes_out = kernel_write(i_fp, g_data_cap, NUM_SAMPLES_TO_RECORD*sizeof(int32_t), &pos);
    RTSLOG_INFO("Wrote %ld bytes to the file\n", bytes_out);

    filp_close(i_fp, NULL);
}


void rt_run_adc_dma( void )
{
    uint64_t gps_ns, gps_last_ns = 0, diff_ns, num_good_adc=0, max_diff_ns = 0, last_dac_error = 0, num_good_adc_dac_txs=0;
    int64_t  last_good_dac= -1;
    uint64_t time_tsc = 0, total_time_ns=0;
    ligo28ao32_error_t dac_ret;

    

    //Wait for stable timing
    while(num_good_adc < 300) {
        udelay(10000);
        ++num_good_adc;
    }
    num_good_adc = 0;

    ligo28ao32_dma_clear_adc_error_ctr(dac_dev);
    ligo28ao32_dma_clear_dac_error_ctr(dac_dev);
    ligo28ao32_dma_read_common_errors(dac_dev); //Clear errors, for read on next line
    //RTSLOG_INFO("ligo28ao32_dma_read_common_errors(dac_dev): %u\n", ligo28ao32_dma_read_common_errors(dac_dev));


    //ligo28ao32_print_sampling_config(dac_dev);
    ligo28ao32_dma_config(dac_dev, 
                          65536, //ADC/DAC Sample rate Hz
                          65536, //ADC/DAC Dma rate Hz
                          0xFFFFFFFF,//ADC sample delay
                          0x3FF, //ADC DMA delay
                          32767, //DAC sample delay
                          32767 //Dac DMA delay
    );
    ligo28ao32_print_converter_config(dac_dev); 
    //ligo28ao32_print_sampling_setup(dac_dev);

    if ( !ligo28ao32_is_adc_config_valid(dac_dev) || !ligo28ao32_is_locked(dac_dev)) { //Valid config, start DMA
        RTSLOG_INFO("ADC config not valid (%u), or DAC timing unlocked (is_locked: %u). Exiting.\n",
        ligo28ao32_is_adc_config_valid(dac_dev), ligo28ao32_is_locked(dac_dev));
        while(atomic_read(&g_atom_should_exit) == 0 ) {};
        atomic_set(&g_atom_has_exited, 1);
        return;
    }

    //Start the DAC DMAs
    udelay(19999);
    ligo28ao32_dma_start_adcs(dac_dev);
    //ligo28ao32_dma_start_dacs(dac_dev);
    udelay(19999);
    
    timer_start( &total_time_ns );

    //Read time from buffer
    dac_ret = ligo28ao32_lock_next_adc_buffer(dac_dev, 20);
    if( dac_ret != LIGO32AO32_OK ) {
        RTSLOG_INFO("Error: Timeout waiting for first ADC DMA transfer.\n");
        goto isolated_exit;
    }
    gps_last_ns = ligo28ao32_get_locked_ts_ns(dac_dev);

    //RTSLOG_INFO("First gps time read: %llu\n", gps_last_ns);

    while( atomic_read(&g_atom_should_exit) == 0 ) {

        timer_start( &time_tsc );

        dac_ret = ligo28ao32_lock_next_adc_buffer(dac_dev, 20);
        if(dac_ret != LIGO32AO32_OK) {
            RTSLOG_INFO("Error: Timeout waiting for ADC DMA transfer. num_good_adc: %llu\n", num_good_adc);
            goto isolated_exit;
        }
        gps_ns = ligo28ao32_get_locked_ts_ns(dac_dev);
        diff_ns = gps_ns - gps_last_ns;
        hist_add_element(g_hist_ctx_raw, diff_ns);
        if(diff_ns > 16000 && num_good_adc != 0) {
            //RTSLOG_INFO("Error: ADC Timestamp jump detected, diff_ns : %llu, last gps: %llu, cur gps: %llu, num_good_adc: %llu\n", 
            //diff_ns, gps_last_ns, gps_ns, num_good_adc);

            RTSLOG_INFO("Error: ADC Timestamp jump detected, diff_ns : %llu, last gps-ticks: %llu-%llu, cur gps-ticks: %llu-%llu, num_good_adc: %llu\n", 
            diff_ns, 
            GET_SEC_FROM_GPS_NS(gps_last_ns), 
            convert_ns_to_4GHz(GET_NANO_FROM_GPS_NS(gps_last_ns))>>16,
            GET_SEC_FROM_GPS_NS(gps_ns), 
            convert_ns_to_4GHz(GET_NANO_FROM_GPS_NS(gps_ns))>>16,
            num_good_adc);
    
            //goto isolated_exit;
        }


        if(diff_ns > max_diff_ns) max_diff_ns = diff_ns;
        ++num_good_adc;
        gps_last_ns = gps_ns;

        uint32_t dma_buf_stat_hold = ligo28ao32_get_locked_status(dac_dev);
        if ((dma_buf_stat_hold & 0xF00) != 0 )
        {
            RTSLOG_ERROR("ADC Error detected! Exiting...\n");
            break;
        }
        else {

            
            int32_t* adc_samples = ligo28ao32_get_locked_samples_ptr(dac_dev);
            if( num_good_adc > 10000 && num_good_adc < 10000+NUM_SAMPLES_TO_RECORD)
            {
                //RTSLOG_RAW("%d\n", adc_samples[0]);
                g_data_cap[num_good_adc-10001] = adc_samples[0] - 16384;
            }

            ++num_good_adc_dac_txs;
            last_good_dac = num_good_adc;
        }

        hist_add_element(g_hist_ctx_cpu, timer_end_ns( &time_tsc ));
        if( time_tsc > 25000)
        {
            RTSLOG_INFO("Got a long wait in cpu time: %lu ns, cur sample cnt: %lu\n", time_tsc, num_good_adc);
        }




    } //while( atomic_read(&g_atom_should_exit) == 0 ) 

    timer_end_ns( &total_time_ns );

    //RTSLOG_INFO("Before DAC stop: \n");
    //ligo28ao32_print_dma_buffer_status(dac_dev, ligo28ao32_get_locked_status(dac_dev));
    //ligo28ao32_dma_stop_dacs(dac_dev);
    udelay(19999); 
    udelay(19999); 
    //RTSLOG_INFO("DACs have been stopped...\n");




    RTSLOG_INFO("num_good_adc: %llu, last diff_ns: %llu, max_diff_ns: %llu \n", num_good_adc, diff_ns, max_diff_ns);
    RTSLOG_INFO("Num good DAC tx: %llu, num BAD DAC: %llu, last DAC error in DMA status: %llu, last_good_dac: %lld\n", 
           num_good_adc_dac_txs, num_good_adc-num_good_adc_dac_txs, last_dac_error, last_good_dac);
    RTSLOG_INFO("Calculated avg DAC error rate : %u/s\n", (num_good_adc-num_good_adc_dac_txs)/(total_time_ns/1000000000));
    //RTSLOG_INFO("Minimum frac time recorded was: %u ns \n", min_frac_ns);
    //RTSLOG_INFO(KERN_INFO "Last ligo28ao32_get_locked_status(): %u\n", ligo28ao32_get_locked_status(dac_dev));
    ligo28ao32_print_dma_buffer_status(dac_dev, ligo28ao32_get_locked_status(dac_dev));

    isolated_exit:
    while(atomic_read(&g_atom_should_exit) == 0 ) {};
    atomic_set(&g_atom_has_exited, 1);
    return;
}



static int __init ld32_init(void) {
 
    int64_t ranges [] = {300, 500, 800, 1200, 2000, 4000, 5000, 6000, 7000, 8000, 9000,
                         10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 20000, 22000, 24000, 26000, 28000, 
                         30000, 33000, 36000, 40000, 60000, 80000, 110000,
                         130000, 150000, 200000, 300000, 400000, 500000, 600000};
    g_hist_ctx_raw = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    g_hist_ctx_cpu = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    if( g_hist_ctx_raw == 0 || g_hist_ctx_cpu == 0) return -1;


    RTSLOG_INFO("Start timing card...\n");
    timing_dev = ligoPTC_configureDevice( 0 );
    if ( !timing_dev ) {
        RTSLOG_INFO("ligoPTC_configureDevice() failed, exiting.");
        return -1;
    }
    RTSLOG_INFO("Timing Card is_locked: %u\n", ligoPTC_is_locked(timing_dev));



    RTSLOG_INFO("Starting to configure DAC card...\n");
    dac_dev = ligo28ao32_configureDevice( DAC_PCIE_INSTANCE );
    if ( !dac_dev ) {
        RTSLOG_INFO("ligo28ao32_configureDevice() failed, exiting.");
        return -1;
    }

    RTSLOG_INFO("Reported slot : 0x%x\n", ligo28ao32_get_backplane_slot(dac_dev) );

    //Pass the clk signal to the DAC board
    int count = 0;
    if (ligo28ao32_is_locked(dac_dev) == 0)
    {
        ligoPTC_set_slot_config(timing_dev, IOC_SLOT, LPTC_SCR_TIM_SIG | LPTC_SCR_LVDS);
        while (ligo28ao32_is_locked(dac_dev) == 0 && count++ < 100) {
            msleep(100);
        }
    }

    if( ligo28ao32_is_locked(dac_dev) == 0) {
        RTSLOG_ERROR("Did not lock...\n");
        goto cleanup_cards;
    }

    ligoPTC_set_bp_config(timing_dev, 0);//Clear out config from advligo
    //ligoPTC_set_bp_config(timing_dev, 0xC); //Set to advligoRTS config
    ligoPTC_print_card_config(timing_dev);
    RTSLOG_INFO("ligoPTC_get_fw_version: 0x%x\n", ligoPTC_get_fw_version(timing_dev));


    //ligo28ao32_print_converter_config(dac_dev); 

    if( ligo28ao32_get_num_adc_chans(dac_dev) < 32 ) {
        RTSLOG_ERROR("Unexpected number of ADC chans: %d, exiting\n");
        goto cleanup_cards;
    }

    
    //Start isolated control loop
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    RTSLOG_INFO("ligo28ao32 driver : Locking free CPU core\n" );

    ret = rts_isolator_run( rt_run_adc_dma, -1, &g_core_used);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : %s\n", error_msg);
        return -1;
    }
    


    RTSLOG_INFO("ligo28ao32 insert done.\n");
    return 0; //END of nominal case


cleanup_cards:
    if( dac_dev ) {
        ligo28ao32_freeDevice( dac_dev );
    }

    if( timing_dev ) {
        ligoPTC_freeDevice(timing_dev);
    }
    return -1;

}


static void __exit ld32_exit(void) {

    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    uint64_t stop_sig_time_ns = get_monotonic_time_ns();

    ligo28ao32_print_sampling_config(dac_dev);

    
    // Stop the code and wait
    atomic_set(&g_atom_should_exit, 1);
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    RTSLOG_INFO("It took %lld ms for the RT code to exit.\n",
                 (get_monotonic_time_ns() - stop_sig_time_ns)/1000000);

    //At this point the CPU will be down...
    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }

    ligo28ao32_dma_stop_adcs(dac_dev);
    ligo28ao32_dma_stop_dacs(dac_dev);
    
    udelay(19999); //Waited for any started transfer

    if( dac_dev ) {
        ligo28ao32_freeDevice( dac_dev );
    }

    if( timing_dev ) {
        ligoPTC_freeDevice(timing_dev);
    }

    udelay(19999);
    udelay(19999); 
    udelay(19999); 


    //printk("Histogram Of All ADC Frac Seconds (ns)\n");
    //hist_print_stats(g_hist_ctx_raw);
    //hist_free(g_hist_ctx_raw);

    printk("Histogram Of All ADC Wait Times by CPU Time (ns)\n");
    hist_print_stats(g_hist_ctx_cpu);
    hist_free(g_hist_ctx_cpu);

    write_data_to_file();

	RTSLOG_INFO("Unloaded ld32 driver!\n");

}

module_init(ld32_init);
module_exit(ld32_exit);
