#include "ligo28ao32.h"
#include "ligo28ao32_private.h"
#include "ligo_fw_utils.h"
#include "pg195.h" //pg195_dma_ctrl_regs_t
#include "common_utils.h"

//#include <asm-generic/iomap.h>
#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/delay.h>



//rts-logger
#include "drv/rts-logger.h"


//TODO: Remove
#define ADC_DMA_BUFFER_LEN 4096
//Settable DAC Parameters
#define DAC_NUM_CYCLES_TO_WRITE_AHEAD 1
#define LOG_2_NUM_DMA_BUFFERS_PER_CHAN 2 //2^1 is 2
//Calculated DAC Parameters
#define DAC_NUM_DMA_BUFFERS ((1<<LOG_2_NUM_DMA_BUFFERS_PER_CHAN) * 2) //Times 2, one set for each DMA chan
#define DAC_DMA_BUFFER_LEN (sizeof(ligo_dac_buffer_section_t) * DAC_NUM_DMA_BUFFERS)



struct ligo28ao32_dev_t * ligo28ao32_configureDevice(unsigned instance)
{
    int pci_status;
    ligo28ao32_dev_t * ctx = ( ligo28ao32_dev_t *) kzalloc(sizeof( ligo28ao32_dev_t ), GFP_KERNEL);
    if( !ctx ) {
        RTSLOG_INFO("ligoPTC_configureDevice(): Failed to allocate context\n");
        return NULL;
    }

    //Search for LIGO converter card on the PCI bus
    struct pci_dev * tmp_pci_dev = NULL;
    int cur_inst = -1;
    while ( (tmp_pci_dev = pci_get_device(L28AO_VENDOR_ID, L28AO_DEVICE_ID, tmp_pci_dev)) )
    {
        ++cur_inst;
        if( cur_inst == (int)instance) {
            ctx->pci_dev = tmp_pci_dev;
            RTSLOG_INFO("ligo28ao32_configureDevice(): Found the device %x:%x driver: %llx\n", tmp_pci_dev->vendor, 
                                                        tmp_pci_dev->device, 
                                                        (uint64_t)tmp_pci_dev->driver);
            break;
        }
    }
    if ( !ctx->pci_dev ) {
        RTSLOG_INFO("ligo28ao32_configureDevice(): Did not find instance %u on the PCI bus, cur_inst: %d\n", instance, cur_inst);
        goto cleanup_alloc;
    }


    pci_status = pci_enable_device( ctx->pci_dev );
    if ( pci_status != 0 ) {
        dev_err(&(ctx->pci_dev->dev), "pci_enable_device\n");
        goto cleanup_alloc;
    }

    pci_set_master( ctx->pci_dev );

    char region_name[100];
    snprintf(region_name, 100, "ligo28ao32_region%d", instance);
    if (pci_request_region(ctx->pci_dev, BAR0_INDX, region_name)) {
		dev_err(&(ctx->pci_dev->dev), "pci_request_region\n");
		goto disable_dev;
	}

    snprintf(region_name, 100, "ligo28ao32_xilinx_dma_region%d", instance);
    if (pci_request_region(ctx->pci_dev, BAR2_INDX, region_name)) {
		dev_err(&(ctx->pci_dev->dev), "pci_request_region\n");
		goto cleanup_card_region;
	}


    //Map registers
    ctx->control_regs_len = pci_resource_len(ctx->pci_dev, BAR0_INDX);
    //RTSLOG_INFO("The BAR0 resource len is %llu\n", pci_resource_len(ctx->pci_dev, BAR0_INDX));
    ctx->ctrl_mm = pci_iomap(ctx->pci_dev, BAR0_INDX, ctx->control_regs_len);
    ctx->xilinx_dma_mm = pci_iomap(ctx->pci_dev, BAR2_INDX, pci_resource_len(ctx->pci_dev, BAR2_INDX));
    //RTSLOG_INFO("pci_resource_len(ctx->pci_dev, BAR2_INDX): %llu\n", pci_resource_len(ctx->pci_dev, BAR2_INDX));
    //RTSLOG_INFO("The BAR2 resource len is %llu\n", pci_resource_len(ctx->pci_dev, BAR2_INDX));
    RTSLOG_INFO("ligo28ao32 : firmware_version : 0x%x\n", ioread32(&ctx->ctrl_mm->firmware_version));



	if (dma_set_mask_and_coherent(&(ctx->pci_dev->dev), DMA_BIT_MASK(64))) {
		dev_warn(&(ctx->pci_dev->dev), "ligo28ao32: No suitable DMA available\n");
		goto cleanup_dma_region;
	}


	ctx->adc_dma_buffer = dma_alloc_coherent(&(ctx->pci_dev->dev), 
                                  ADC_DMA_BUFFER_LEN, 
                                  &ctx->adc_dma_bus_addr, 
                                  GFP_KERNEL);
    //RTSLOG_INFO("The DMA bus addr is %llx\n", (uint64_t)ctx->adc_dma_bus_addr);
    if(ctx->adc_dma_buffer == NULL) {
        RTSLOG_INFO("Failed to alloc the coherent ADC DMA buffer...\n");
        goto cleanup_dma_region;
    }

    ctx->dac_dma_buffer = dma_alloc_coherent(&(ctx->pci_dev->dev), 
                                  DAC_DMA_BUFFER_LEN, 
                                  &ctx->dac_dma_bus_addr, 
                                  GFP_KERNEL);
    if(ctx->dac_dma_buffer == NULL) {
        RTSLOG_INFO("Failed to alloc the coherent DAC DMA buffer...\n");
        goto cleanup_adc_buffer;
    }                              

    //RTSLOG_INFO("offsetof dac_data_delay: %lu\n", offsetof(ligo28ao32_ctrl_regs, dac_data_delay));
    
    iowrite32(0x08000000, &ctx->xilinx_dma_mm->card_to_host_chan_0.control_w1s); //Enable ADC DMA chan 0
    iowrite32(0x08000000, &ctx->xilinx_dma_mm->card_to_host_chan_1.control_w1s); //Enable ADC DMA chan 1
    iowrite32(0x08000000, &ctx->xilinx_dma_mm->host_to_card_chan_0.control_w1s); //Enable DAC DMA chan 0
    iowrite32(0x08000000, &ctx->xilinx_dma_mm->host_to_card_chan_1.control_w1s); //Enable DAC DMA chan 1

    //RTSLOG_INFO("offsetof AIN_OR_ERROR: %lu\n", offsetof(ad4134_regs_t, AIN_OR_ERROR));


    //Init context vars
    ctx->last_locked_ts_ns = 0;
    ctx->cur_dac_tick = 0;
    ctx->cur_dac_buffer = 0;
    ctx->dac_num_dma_buffers = DAC_NUM_DMA_BUFFERS; 
    ctx->last_dac_tx_ts_ns =  0;
    //Keeps track if our dma delay is larger than our sample delay
    //When this is the case the timestamp needs to be + 1 cycle but same buffer location
    ctx->dac_write_next_ts = 0; 

    return ctx; //Nominal return


    //
    // Start error returns
    cleanup_adc_buffer:
    dma_free_coherent(&(ctx->pci_dev->dev), 
                      ADC_DMA_BUFFER_LEN, 
                      ctx->adc_dma_buffer, 
                      ctx->adc_dma_bus_addr);
    cleanup_dma_region:
    pci_release_region(ctx->pci_dev, BAR2_INDX);
    cleanup_card_region:
    disable_dev:
    if( pci_is_enabled( ctx->pci_dev ) ) pci_disable_device(ctx->pci_dev);
    pci_release_region(ctx->pci_dev, BAR0_INDX);
    cleanup_alloc:
    kfree(ctx);
    return NULL;
}

void ligo28ao32_freeDevice(struct ligo28ao32_dev_t * ctx)
{

    dma_free_coherent(&(ctx->pci_dev->dev), 
                      DAC_DMA_BUFFER_LEN, 
                      ctx->dac_dma_buffer, 
                      ctx->dac_dma_bus_addr);

    dma_free_coherent(&(ctx->pci_dev->dev), 
                      ADC_DMA_BUFFER_LEN, 
                      ctx->adc_dma_buffer, 
                      ctx->adc_dma_bus_addr);

    if( pci_is_enabled( ctx->pci_dev ) ) pci_disable_device(ctx->pci_dev);

    pci_release_region(ctx->pci_dev, BAR0_INDX);
    pci_release_region(ctx->pci_dev, BAR2_INDX);

    kfree(ctx);
    RTSLOG_INFO("ligo28ao32_freeDevice() complete.\n");
}

double ligo28ao32_get_time_sec(ligo28ao32_dev_t * dev)
{
    double frac_sec = ((double)ioread32(&dev->ctrl_mm->time_frac) / (1ULL<<32));
    return (ioread32(&dev->ctrl_mm->time_sec) + frac_sec);
}

uint64_t ligo28ao32_get_time_ns(ligo28ao32_dev_t * dev)
{
    uint32_t frac_ns = convert_4GHz_to_ns( ioread32(&dev->ctrl_mm->time_frac) );
    return (ioread32(&dev->ctrl_mm->time_sec) * 1e9) + frac_ns;
}

int ligo28ao32_is_locked(ligo28ao32_dev_t * dev)
{
    return GET_BIT(ioread32(&dev->ctrl_mm->status_bits), STAT_INT_TIMING_LOCKED);
}

int ligo28ao32_get_backplane_slot(ligo28ao32_dev_t * dev)
{
    uint32_t node_addr = ioread32(&dev->ctrl_mm->node_addr);
    //The bits of node address are not documented in the 
    //converter board register mem map.

    if( node_addr == 0 ) {
        RTSLOG_WARN("ligo28ao32_get_backplane_slot() Called on, but node is Master.\n");
        return -1;
    }
    if (( node_addr & 0x80000000) != 0 ) {
        RTSLOG_WARN("ligo28ao32_get_backplane_slot() Called on, but node is invalid.\n");
        return -1;
    }

    uint32_t shifts =  (7 - ((node_addr & 0x70000000) >> 28)) * 4;
    uint32_t byte_mask = 0xF << shifts;

    return ((node_addr & byte_mask) >> shifts);

}


//
//ADC Functions
//
int ligo28ao32_get_num_adc_chans(ligo28ao32_dev_t * dev)
{
    return ioread32(&dev->ctrl_mm->num_adc_channels);
}

int ligo28ao32_is_adc_config_valid(ligo28ao32_dev_t * dev)
{
    return GET_BIT(ioread32(&dev->ctrl_mm->samp_status), SAMP_STAT_ADC_VALID_SAMP_CONFIG);
}

int ligo28ao32_is_adc_dma_running(ligo28ao32_dev_t * dev)
{
    return GET_BIT(ioread32(&dev->ctrl_mm->samp_status), SAMP_STAT_ADC_DMA_RUNNING);
}

int ligo28ao32_is_adc_converter_running(ligo28ao32_dev_t * dev)
{
    return GET_BIT(ioread32(&dev->ctrl_mm->samp_status), SAMP_STAT_ADC_CONVERT_RUNNING);
}

uint32_t ligo28ao32_get_adc_buf_status(ligo28ao32_dev_t * dev)
{
    ligo_adc_buffer_section_t * buffer_ptr = (ligo_adc_buffer_section_t *) dev->adc_dma_buffer;
    return ioread32(&buffer_ptr->status);
}

void ligo28ao32_consume_adc_buffer(ligo28ao32_dev_t * dev)
{
    ligo_adc_buffer_section_t * buffer_ptr = (ligo_adc_buffer_section_t *) dev->adc_dma_buffer;
    iowrite32(0xFFFFFFFF,  &buffer_ptr->overflows);
}

ligo28ao32_error_t ligo28ao32_wait_adc_dma(ligo28ao32_dev_t * dev, uint32_t max_wait_us, uint64_t * gps_ts_ns)
{

    uint64_t timer_tsc;
    uint32_t timed_out = 0;
    ligo_adc_buffer_section_t * buffer_ptr = (ligo_adc_buffer_section_t *) dev->adc_dma_buffer;
    timer_start(&timer_tsc);
    while ( ioread32(&buffer_ptr->overflows) == 0xFFFFFFFF )
    {
        if( timer_tock_ns(&timer_tsc)/1000 > max_wait_us) {
            timed_out = 1;
            break;
        }
    }

    if( timed_out ) return LIGO32AO32_TIMEOUT;

    if( gps_ts_ns )
        *gps_ts_ns = convert_4GHz_to_ns(ioread32(&buffer_ptr->time_232_clk)) + 
                    (ioread32(&buffer_ptr->time_sec) * 1e9);

    return LIGO32AO32_OK;

}

ligo28ao32_error_t ligo28ao32_lock_next_adc_buffer(ligo28ao32_dev_t * dev, uint32_t max_wait_us)
{
    uint64_t res_ns;
    uint32_t num_waits = 0;
    volatile ligo_adc_buffer_section_t * buffer_ptr = (ligo_adc_buffer_section_t *) dev->adc_dma_buffer;


    buffer_ptr->overflows = 0xFFFFFFFF;
    //We could miss a sample if we don't set the above line and start listening 
    //before the next RX
    while ( buffer_ptr->overflows == 0xFFFFFFFF && num_waits < max_wait_us * 100000000)
    {
        //udelay(1);
        ++num_waits;
    }
    res_ns =  convert_4GHz_to_ns(buffer_ptr->time_232_clk) + (buffer_ptr->time_sec * 1e9);

    /* OLD logic, had issue with sec changing before frac sec
    res_ns = convert_4GHz_to_ns(buffer_ptr->time_232_clk) + (buffer_ptr->time_sec * 1e9);
    while (res_ns == dev->last_locked_ts_ns && num_waits < max_wait_us * 100000000)
    {
        //udelay(1);
        res_ns =  convert_4GHz_to_ns(buffer_ptr->time_232_clk) + (buffer_ptr->time_sec * 1e9);
        ++num_waits;
    }*/


    if(num_waits == max_wait_us) return LIGO32AO32_TIMEOUT;
    else dev->last_locked_ts_ns = res_ns;

    //TODO: Status and overflow might not be set yet
    memcpy(&dev->locked_adc_buf, (void*)buffer_ptr, sizeof(dev->locked_adc_buf));

    return LIGO32AO32_OK;
}

uint64_t ligo28ao32_get_locked_ts_ns(ligo28ao32_dev_t * dev)
{
    return dev->last_locked_ts_ns;
}

uint64_t ligo28ao32_get_locked_ts_raw(ligo28ao32_dev_t * dev)
{
    return (((uint64_t)dev->locked_adc_buf.time_sec) << 32) | dev->locked_adc_buf.time_232_clk;
}

uint32_t ligo28ao32_get_locked_status(ligo28ao32_dev_t * dev)
{
    return dev->locked_adc_buf.status;
}

uint32_t ligo28ao32_get_locked_overflow(ligo28ao32_dev_t * dev)
{
    return dev->locked_adc_buf.overflows;
}

int32_t * ligo28ao32_get_locked_samples_ptr(ligo28ao32_dev_t * dev)
{
    return dev->locked_adc_buf.samples;
}

//
//DAC Functions
//
int ligo28ao32_get_num_dac_chans(ligo28ao32_dev_t * dev)
{
    return ioread32(&dev->ctrl_mm->num_dac_channels);
}

int ligo28ao32_is_dac_config_valid(ligo28ao32_dev_t * dev)
{
    return GET_BIT(ioread32(&dev->ctrl_mm->samp_status), SAMP_STAT_DAC_VALID_SAMP_CONFIG);
}

int ligo28ao32_is_dac_dma_running(ligo28ao32_dev_t * dev)
{
    return GET_BIT(ioread32(&dev->ctrl_mm->samp_status), SAMP_STAT_DAC_DMA_RUNNING);
}


ligo28ao32_error_t ligo28ao32_queue_next_dac_dma(ligo28ao32_dev_t * dev, uint32_t num_samples, uint32_t samples [])
{
    //static int index = 0;

    if (dev->last_locked_ts_ns == 0) {
        RTSLOG_INFO("ligo28ao32_queue_next_dac_dma(): Queue called, but ADC not running.");
        return LIGO32AO32_DAC_ERROR;
    }

    if (num_samples > LIGO_28AO32_NUM_DACS) {
        RTSLOG_INFO("ligo28ao32_queue_next_dac_dma(): Queue called with %u samples, but dac has only %u channels\n",
        LIGO_28AO32_NUM_DACS);
        return LIGO32AO32_DAC_ERROR;
    }

    if ( samples == NULL ) {
        RTSLOG_INFO("ligo28ao32_queue_next_dac_dma(): Queue called but samples pointer is null.\n",
        LIGO_28AO32_NUM_DACS);
        return LIGO32AO32_DAC_ERROR;
    }

    //Read the timestamp from the ADC DMA buffer and generate a DAC timestamp for the next sample
    uint64_t dac_tx_gps_ns = dev->last_locked_ts_ns;
    uint32_t cur_dac_tick = convert_ns_to_4GHz(GET_NANO_FROM_GPS_NS(dac_tx_gps_ns)) ;
    if( (cur_dac_tick & 0x0000FFFF) > 32768) cur_dac_tick = (cur_dac_tick + 65536);
    cur_dac_tick &= 0xFFFF0000;

    //To 65k clk domain
    cur_dac_tick = cur_dac_tick >> 16;

    if ( (cur_dac_tick + dev->dac_write_next_ts) > (1<<16) - (DAC_NUM_CYCLES_TO_WRITE_AHEAD+1) ) 
        dac_tx_gps_ns += 1000000000; 
    cur_dac_tick =  (cur_dac_tick + DAC_NUM_CYCLES_TO_WRITE_AHEAD + dev->dac_write_next_ts) % (1<<16);

    uint32_t cur_dac_dma_buffer = (cur_dac_tick - dev->dac_write_next_ts)  % DAC_NUM_DMA_BUFFERS;
    volatile ligo_dac_buffer_section_t * buffer_ptr = (ligo_dac_buffer_section_t *)
                                        ((uint8_t*)dev->dac_dma_buffer + (cur_dac_dma_buffer)
                                        * sizeof(ligo_dac_buffer_section_t));
    buffer_ptr->time_sec = GET_SEC_FROM_GPS_NS(dac_tx_gps_ns);
    buffer_ptr->time_232_clk = cur_dac_tick << 16; //Back to 2^32 domain
    

    memcpy((void*)&buffer_ptr->samples, samples, num_samples);

    if ( dev->last_dac_tx_ts_ns == 0 ) {
        ligo28ao32_dma_start_dacs(dev);
        dev->last_dac_tx_ts_ns = dev->last_locked_ts_ns;
    }

    /*
    if(index < 10) {
        RTSLOG_INFO("last_locked_ts just ns: %llu, set DAQ TX time (ns): %llu\n", GET_NANO_FROM_GPS_NS(dev->last_locked_ts_ns),  convert_4GHz_to_ns(cur_dac_tick));
        RTSLOG_INFO("last_locked_ts (2^32): %u, set DAQ TX time (2^32): %u\n", dev->locked_adc_buf.time_232_clk>>16,  cur_dac_tick>>16);
        ++index;
    }*/


    return LIGO32AO32_OK;
}

void * ligo28ao32_get_next_dac_dma_queue(ligo28ao32_dev_t * dev, uint32_t gps_time_s, uint32_t cur_cycle_64kclk)
{


    if( (cur_cycle_64kclk + dev->dac_write_next_ts) > (1<<16) - (DAC_NUM_CYCLES_TO_WRITE_AHEAD+1) ) {
        ++gps_time_s;   
    }
    cur_cycle_64kclk =  (cur_cycle_64kclk + DAC_NUM_CYCLES_TO_WRITE_AHEAD + dev->dac_write_next_ts) % (1<<16) ;


    ligo_dac_buffer_section_t * buffer_ptr = (ligo_dac_buffer_section_t *)  
                                        ((uint8_t*)dev->dac_dma_buffer + ((cur_cycle_64kclk - dev->dac_write_next_ts) % DAC_NUM_DMA_BUFFERS)
                                        * sizeof(ligo_dac_buffer_section_t));
    buffer_ptr->time_sec = gps_time_s;
    buffer_ptr->time_232_clk = cur_cycle_64kclk << 16; //To 2^32 clk domain

    if (dev->dac_first_dma == 0){
        ligo28ao32_dma_start_dacs(dev);
        ++dev->dac_first_dma;
    } 


    return (void*)&buffer_ptr->samples;
}

//
//DMA Functions
//

void ligo28ao32_dma_config(ligo28ao32_dev_t * dev, unsigned sample_rate_hz, unsigned dma_rate_hz,
                           unsigned adc_sample_delay, unsigned adc_dma_delay,
                           unsigned dac_sample_delay, unsigned dma_dma_delay)
{

    //Get the ADC/DAC dma length
    uint32_t adc_dma_len_bytes = ligo28ao32_get_adc_dma_length(dev);
    uint32_t dac_dma_len_bytes = sizeof(ligo_dac_buffer_section_t);


    //Set up the ADC sampling parameters
    iowrite32(sample_rate_hz - 1, &dev->ctrl_mm->adc_sample_period);
    iowrite32(adc_sample_delay, &dev->ctrl_mm->adc_sample_delay);
    iowrite32(dma_rate_hz - 1, &dev->ctrl_mm->adc_dma_period);
    iowrite32(adc_dma_delay , &dev->ctrl_mm->adc_dma_delay );

    ligo28ao32_dma_config_adc(dev, adc_dma_len_bytes, 0);


    //Set up the DAC sampling parameters
    iowrite32(sample_rate_hz - 1, &dev->ctrl_mm->dac_sample_period);
    dev->dac_sample_delay = dac_sample_delay;
    iowrite32(dev->dac_sample_delay, &dev->ctrl_mm->dac_sample_delay);
    iowrite32(dma_rate_hz - 1, &dev->ctrl_mm->dac_dma_period);
    dev->dac_dma_delay = dma_dma_delay;
    iowrite32(dev->dac_dma_delay, &dev->ctrl_mm->dac_dma_delay );

    if (dev->dac_dma_delay >= dev->dac_sample_delay) dev->dac_write_next_ts = 1;

    ligo28ao32_dma_config_dac(dev, dac_dma_len_bytes, 2*dac_dma_len_bytes);

    uint32_t dma_config_reg = ioread32(&dev->ctrl_mm->samp_config);
    //RTSLOG_INFO("samp_config: 0x%x\n", dma_config_reg);
    dma_config_reg &= ~SAMP_CONF_LOG2_NUM_BUF_PER_DAC_DMA_CHAN; //Clear out any old config
    dma_config_reg |=  (LOG_2_NUM_DMA_BUFFERS_PER_CHAN << 24); //Set two buffer per chan 
    iowrite32(dma_config_reg, &dev->ctrl_mm->samp_config);
    //RTSLOG_INFO("samp_config: 0x%x\n", dma_config_reg);

    //Read and program the log2 number of dma buffers
    /* TODO: Not needed for 64K sample/dma
    uint32_t temp = ioread32(&dev->ctrl_mm->buffs_per_dma_and_clk);
    uint32_t log2maxdmabuffers = (temp >> 8) & 0xFF;
    if (log2maxdmabuffers > 3) log2maxdmabuffers = 3;
    uint32_t dma_config_reg = ioread32(&dev->ctrl_mm->samp_config);
    dma_config_reg &= 0x00FF00FF; //Clear the bits we are going to set
    dma_config_reg |= ((log2maxdmabuffers & 0x000000FF) << 24) | ((log2maxdmabuffers & 0x000000FF) << 8);
    iowrite32(dma_config_reg, &dev->ctrl_mm->samp_config);*/

}


void ligo28ao32_dma_start_adcs(ligo28ao32_dev_t * dev)
{
    //Enable ADC DMA
    setBitIndex(SAMP_CONF_ENABLE_ADC_DMA, &dev->ctrl_mm->samp_config);

    iowrite32(0x00000001, &dev->xilinx_dma_mm->card_to_host_chan_0.control_w1s);
    iowrite32(0x00000001, &dev->xilinx_dma_mm->card_to_host_chan_1.control_w1s);
}

void ligo28ao32_dma_stop_adcs(ligo28ao32_dev_t * dev)
{
    //Disable ADC DMA
    clearBitIndex(SAMP_CONF_ENABLE_ADC_DMA, &dev->ctrl_mm->samp_config);

    iowrite32(0x00000001, &dev->xilinx_dma_mm->card_to_host_chan_0.control_w1c);
    iowrite32(0x00000001, &dev->xilinx_dma_mm->card_to_host_chan_1.control_w1c);
}

void ligo28ao32_dma_start_dacs(ligo28ao32_dev_t * dev)
{
    //Enable ADC DMA
    setBitIndex(SAMP_CONF_ENABLE_DAC_DMA, &dev->ctrl_mm->samp_config);

    iowrite32(0x00000001, &dev->xilinx_dma_mm->host_to_card_chan_0.control_w1s);
    iowrite32(0x00000001, &dev->xilinx_dma_mm->host_to_card_chan_1.control_w1s);
}

void ligo28ao32_dma_stop_dacs(ligo28ao32_dev_t * dev)
{
    //Disable ADC DMA
    clearBitIndex(SAMP_CONF_ENABLE_DAC_DMA, &dev->ctrl_mm->samp_config);

    iowrite32(0x00000001, &dev->xilinx_dma_mm->host_to_card_chan_0.control_w1c);
    iowrite32(0x00000001, &dev->xilinx_dma_mm->host_to_card_chan_1.control_w1c);
}

void ligo28ao32_dma_config_adc(ligo28ao32_dev_t * dev, uint32_t dma_len, uint32_t step)
{
    //Configure the even DMA buffer
    //Write toal DMA len, the DMA engine will mod the addr by this after adding the step
    iowrite32(dma_len, &dev->ctrl_mm->dma_chan_config.even_in.length);
    //Write DMA step size, this will be added to the DMA pointer after each DMA, mod the dma_len
    iowrite32(step, &dev->ctrl_mm->dma_chan_config.even_in.buffer_offset);
    if(sizeof(dma_addr_t) > 4) {
        iowrite32((dev->adc_dma_bus_addr >> 32) & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.even_in.addr_msb);
    }
    iowrite32( (uint32_t)(dev->adc_dma_bus_addr & 0xFFFFFFFF), &dev->ctrl_mm->dma_chan_config.even_in.addr_lsb);

    // Configure the odd DMA buffer
    iowrite32(dma_len, &dev->ctrl_mm->dma_chan_config.odd_in.length);
    iowrite32(step, &dev->ctrl_mm->dma_chan_config.odd_in.buffer_offset);
    if(sizeof(dma_addr_t) > 4) {
        iowrite32((dev->adc_dma_bus_addr >> 32) & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.odd_in.addr_msb);
    }
    iowrite32(dev->adc_dma_bus_addr & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.odd_in.addr_lsb);

}


void ligo28ao32_dma_config_dac(ligo28ao32_dev_t * dev, uint32_t dma_len, uint32_t step)
{
    iowrite32(dma_len, &dev->ctrl_mm->dma_chan_config.even_out.length);
    iowrite32(step, &dev->ctrl_mm->dma_chan_config.even_out.buffer_offset);
    if(sizeof(dma_addr_t) > 4) {
        iowrite32((dev->dac_dma_bus_addr >> 32) & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.even_out.addr_msb);
    }
    iowrite32(dev->dac_dma_bus_addr & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.even_out.addr_lsb);

    //Configure the odd 
    uint8_t * buf = (uint8_t *)dev->dac_dma_bus_addr ;
    buf += sizeof(ligo_dac_buffer_section_t);
    dma_addr_t addr = (dma_addr_t ) buf;
    iowrite32(dma_len, &dev->ctrl_mm->dma_chan_config.odd_out.length);
    iowrite32(step, &dev->ctrl_mm->dma_chan_config.odd_out.buffer_offset);
    if(sizeof(dma_addr_t) > 4) {
        iowrite32((addr >> 32) & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.odd_out.addr_msb);
    }
    iowrite32(addr & 0xFFFFFFFF, &dev->ctrl_mm->dma_chan_config.odd_out.addr_lsb);
}

int ligo28ao32_dma_query_adc_error_ctr(ligo28ao32_dev_t * dev)
{
    return ioread32(&dev->ctrl_mm->samp_adc_error_ctr);
}

void ligo28ao32_dma_clear_adc_error_ctr(ligo28ao32_dev_t * dev)
{
    iowrite32(0, &dev->ctrl_mm->samp_adc_error_ctr);
}

int ligo28ao32_dma_query_dac_error_ctr(ligo28ao32_dev_t * dev)
{
    return ioread32(&dev->ctrl_mm->samp_dac_error_ctr);
}

void ligo28ao32_dma_clear_dac_error_ctr(ligo28ao32_dev_t * dev)
{
    iowrite32(0, &dev->ctrl_mm->samp_dac_error_ctr);
}

uint32_t ligo28ao32_dma_read_common_errors(ligo28ao32_dev_t * dev)
{
    return ioread32(&dev->ctrl_mm->samp_status);
}

int ligo28ao32_dma_query_num_buff_per_adc_chan(ligo28ao32_dev_t * dev)
{
    return (ioread32(&dev->ctrl_mm->samp_config) & SAMP_CONF_LOG2_NUM_BUF_PER_ADC_DMA_CHAN) >> 8;
}

//Util Functions

void ligo28ao32_reset_watchdog(ligo28ao32_dev_t * dev)
{
    //RTSLOG_INFO("Write to WD: offset: %d\n", offsetof(ligo28ao32_ctrl_regs, watchdog));
    iowrite32(1, &dev->ctrl_mm->watchdog); //Write any value
}

uint32_t ligo28ao32_get_watchdog(ligo28ao32_dev_t * dev)
{
    return (ioread32(&dev->ctrl_mm->watchdog));
}

int ligo28ao32_samp_stat_is_timing_ok(uint32_t samp_status_reg)
{
    return GET_BIT(samp_status_reg, SAMP_STAT_TIMING_OK);
}

int ligo28ao32_get_temp_c(ligo28ao32_dev_t * dev)
{
    uint64_t temp = ioread32(&dev->ctrl_mm->temp_and_internal_pow_supply_1) 
                             & ON_BOARD_FEATURES_TEMP_MASK;
    temp *= 503975;
    temp /= 65536;
    temp -= 273150;
    temp /= 1000;
    return temp; //In Celsius
}

int ligo28ao32_get_adc_dma_length(ligo28ao32_dev_t * dev)
{
    uint32_t dmaLength = LIGO_28AO32_BYTES_PER_SAMPLE * ligo28ao32_get_num_adc_chans(dev);
    uint32_t dma_buffer_lens = ioread32(&dev->ctrl_mm->dma_buffer_widths);
    uint32_t LogAdcSamplingWdith = dma_buffer_lens & 0xFF;
    uint32_t LogAdcDmaBusWdith   = (dma_buffer_lens >> 8) & 0xFF;
    //uint32_t LogDacSamplingWdith = (dma_buffer_lens >> 16) & 0xFF;
    //uint32_t LogDacDmaBusWdith   = (dma_buffer_lens >> 24) & 0xFF;

    uint32_t samp_width_bytes = 1UL << LogAdcSamplingWdith;
    dmaLength = round_to_line(dmaLength, samp_width_bytes );
    // Multiply by oversampling factor
    //dmalength *= oversampling;

    uint32_t bus_width_bytes = 1UL << LogAdcDmaBusWdith;
    dmaLength = round_to_line(dmaLength, bus_width_bytes );
    dmaLength += 16; //DmaStatusLength status bytes at top of buffer? 
    dmaLength = round_to_line(dmaLength, 64);
    return dmaLength;
}

void ligo28ao32_print_status_registers(ligo28ao32_dev_t * dev)
{
    RTSLOG_INFO("Status and Interrupt Control (Read):\n");
    uint32_t temp = ioread32(&dev->ctrl_mm->status_bits);
    RTSLOG_INFO("Timing OK: %u\n", GET_BIT(temp, STAT_INT_TIMING_LOCKED) );
    RTSLOG_INFO("Uplink is up and working %u\n", GET_BIT(temp, STAT_INT_UPLINK_IS_UP_AND_WORKING) );
    RTSLOG_INFO("VCXO voltage out-of-range: %u\n", GET_BIT(temp, STAT_INT_VCXO_CONTROL_VOLTAGE_OUT_OF_RANGE) );
    RTSLOG_INFO("Leap sec decoded: %u\n", GET_BIT(temp, STAT_INT_LEAP_SEC_DECODED) );
    RTSLOG_INFO("Leap sec subtraction pending: %u\n", GET_BIT(temp, STAT_INT_SUB_LEAP_SEC_PENDING) );
    RTSLOG_INFO("Leap sec addition pending: %u\n", GET_BIT(temp, STAT_INT_ADD_LEAP_SEC_PENDING) );
    RTSLOG_INFO("Watchdog monitor: %u\n", GET_BIT(temp, STAT_INT_WATCHDOG_MON) );

}

void ligo28ao32_print_dma_and_converter_status(ligo28ao32_dev_t * dev)
{
    //volatile uint64_t * buff = (uint64_t *) dev->dma_buffer;
    //RTSLOG_INFO("DMA Buffer : %llx, %llx, %lld, %lld \n", buff[0], buff[1], buff[2], buff[3]);
    //print_hex_dump(KERN_ERR, "  ", DUMP_PREFIX_OFFSET, 16, 1, dev->dma_buffer, DMA_BUFFER_LEN, 0);
    ligo_adc_buffer_section_t * buffer_ptr = (ligo_adc_buffer_section_t *) dev->adc_dma_buffer;
    RTSLOG_INFO("Time from DMA buffer 1 %u:%u\n", buffer_ptr->time_sec, buffer_ptr->time_232_clk);
    buffer_ptr++;
    RTSLOG_INFO("Time from DMA buffer 2 %u:%u\n", buffer_ptr->time_sec, buffer_ptr->time_232_clk);
    buffer_ptr++;
    RTSLOG_INFO("Time from DMA buffer 3 %u:%u\n", buffer_ptr->time_sec, buffer_ptr->time_232_clk);
    buffer_ptr++;
    RTSLOG_INFO("Time from DMA buffer 4 %u:%u\n", buffer_ptr->time_sec, buffer_ptr->time_232_clk);


    /*
    RTSLOG_INFO("ligo28ao32_is_adc_config_valid(): %d\n", ligo28ao32_is_adc_config_valid(dev));
    RTSLOG_INFO("ligo28ao32_is_adc_dma_running(): %d\n", ligo28ao32_is_adc_dma_running(dev));
    RTSLOG_INFO("ligo28ao32_dma_query_adc_errors(): %d\n", ligo28ao32_dma_query_adc_errors(dev));
    RTSLOG_INFO("ligo28ao32_dma_query_num_buff_per_adc_chan(): %d\n", ligo28ao32_dma_query_num_buff_per_adc_chan(dev));
    RTSLOG_INFO("ligo28ao32_is_adc_converter_running(): %d\n", ligo28ao32_is_adc_converter_running(dev));*/
}

void ligo28ao32_print_sampling_config(ligo28ao32_dev_t * dev)
{
    RTSLOG_INFO("Sampling Status (Read Only):\n");
    uint32_t temp = ioread32(&dev->ctrl_mm->samp_status);
    RTSLOG_INFO("DAC timestamp error chan 1: %u\n", GET_BIT(temp, SAMP_STAT_DAC_TIMESTAMP_ERR_CHAN_1) );
    RTSLOG_INFO("DAC timestamp error chan 0: %u\n", GET_BIT(temp, SAMP_STAT_DAC_TIMESTAMP_ERR_CHAN_0) );
    RTSLOG_INFO("DAC DMA missing data 1: %u\n", GET_BIT(temp, SAMP_STAT_DAC_DMA_MISSING_DATA_1) );
    RTSLOG_INFO("DAC DMA missing data 0: %u\n", GET_BIT(temp, SAMP_STAT_DAC_DMA_MISSING_DATA_0) );
    RTSLOG_INFO("DAC DMA ready error 1: %u\n", GET_BIT(temp, SAMP_STAT_DAC_DMA_ERR_CHAN_1) );
    RTSLOG_INFO("DAC DMA ready error 0: %u\n", GET_BIT(temp, SAMP_STAT_DAC_DMA_ERR_CHAN_0) );
    RTSLOG_INFO("Watchdog Mon: %u\n", GET_BIT(temp, SAMP_STAT_WATCHDOG_MON) );
    RTSLOG_INFO("Valid DAC sampling config: %u\n", GET_BIT(temp, SAMP_STAT_DAC_VALID_SAMP_CONFIG));
    RTSLOG_INFO("DAC converter running: %u\n", GET_BIT(temp, SAMP_STAT_DAC_CONVERT_RUNNING));
    RTSLOG_INFO("DAC DMA running: %u\n", GET_BIT(temp, SAMP_STAT_DAC_DMA_RUNNING));

    RTSLOG_INFO("ADC DMA missing data 1: %u\n", GET_BIT(temp, SAMP_STAT_ADC_DMA_MISSING_DATA_1) );
    RTSLOG_INFO("ADC DMA missing data 0: %u\n", GET_BIT(temp, SAMP_STAT_ADC_DMA_MISSING_DATA_0) );
    RTSLOG_INFO("ADC DMA ready error 1: %u\n", GET_BIT(temp, SAMP_STAT_ADC_DMA_ERR_CHAN_1) );
    RTSLOG_INFO("ADC DMA ready error 0: %u\n", GET_BIT(temp, SAMP_STAT_ADC_DMA_ERR_CHAN_0) );
    RTSLOG_INFO("ADC Timing OK: %u\n", GET_BIT(temp, SAMP_STAT_TIMING_OK) );
    RTSLOG_INFO("Valid ADC sampling config: %u\n", GET_BIT(temp, SAMP_STAT_ADC_VALID_SAMP_CONFIG) );
    RTSLOG_INFO("ADC converter running: %u\n", GET_BIT(temp, SAMP_STAT_ADC_CONVERT_RUNNING) );
    RTSLOG_INFO("ADC DMA running: %u\n", GET_BIT(temp, SAMP_STAT_ADC_DMA_RUNNING) );

    temp = ioread32(&dev->ctrl_mm->samp_config);
    RTSLOG_INFO("Sampling Config (Read/Write):\n");
    RTSLOG_INFO("Log2 num buffer per DAC DMA chan: %u\n", (temp & SAMP_CONF_LOG2_NUM_BUF_PER_DAC_DMA_CHAN) >> 24 );
    RTSLOG_INFO("Use interrupt to signal DAC DMA done: %u\n", GET_BIT(temp, SAMP_CONF_USE_INT_TO_SIGNAL_DAC_DONE) );
    RTSLOG_INFO("Ignore DAC timestamp errors: %u\n", GET_BIT(temp, SAMP_CONF_IGNORE_DAC_TIMESTAMP_ERRORS) );
    RTSLOG_INFO("Disable DAC timestamp: %u\n", GET_BIT(temp, SAMP_CONF_DISABLE_DAC_TIMESTAMP) );
    RTSLOG_INFO("Disable DAC conversion: %u\n", GET_BIT(temp, SAMP_CONF_DISABLE_DAC_CONVERSION) );
    RTSLOG_INFO("Enable DAC DMA: %u\n", GET_BIT(temp, SAMP_CONF_ENABLE_DAC_DMA) );

    RTSLOG_INFO("Log2 num buffer per ADC DMA chan: %u\n", (temp & SAMP_CONF_LOG2_NUM_BUF_PER_ADC_DMA_CHAN) >> 8 );
    RTSLOG_INFO("Use interrupt to signal ADC DMA done: %u\n", GET_BIT(temp, SAMP_CONF_USE_INT_TO_SIGNAL_ADC_DONE) );
    RTSLOG_INFO("Disable ADC timestamp: %u\n", GET_BIT(temp, SAMP_CONF_DISABLE_ADC_TIMESTAMP) );
    RTSLOG_INFO("Disable ADC conversion: %u\n", GET_BIT(temp, SAMP_CONF_DISABLE_ADC_CONVERSION) );
    RTSLOG_INFO("Enable ADC DMA: %u\n", GET_BIT(temp, SAMP_CONF_ENABLE_ADC_DMA) );

    RTSLOG_INFO("ADC num late/missing DMA errors: %d\n", ligo28ao32_dma_query_adc_error_ctr(dev));
    RTSLOG_INFO("DAC num late/missing DMA errors: %d\n", ligo28ao32_dma_query_dac_error_ctr(dev));
}

void ligo28ao32_print_sampling_setup(ligo28ao32_dev_t * dev)
{
    RTSLOG_INFO("Sampling Setup (Read/Write):\n");
    RTSLOG_INFO("ADC DMA Period: %u\n", ioread32(&dev->ctrl_mm->adc_dma_period));
    RTSLOG_INFO("ADC DMA Delay: %u\n", ioread32(&dev->ctrl_mm->adc_dma_delay));
    RTSLOG_INFO("ADC Sampling Period: %u\n", ioread32(&dev->ctrl_mm->adc_sample_period));
    RTSLOG_INFO("ADC Sampling Delay: %u\n", ioread32(&dev->ctrl_mm->adc_sample_delay));
    RTSLOG_INFO("DAC DMA Period: %u\n", ioread32(&dev->ctrl_mm->dac_dma_period));
    RTSLOG_INFO("DAC DMA Delay: %u\n", ioread32(&dev->ctrl_mm->dac_dma_delay));
    RTSLOG_INFO("DAC Sampling Period: %u\n", ioread32(&dev->ctrl_mm->dac_sample_period));
    RTSLOG_INFO("DAC Sampling Delay: %u\n", ioread32(&dev->ctrl_mm->dac_sample_delay));
}

void ligo28ao32_print_converter_config(ligo28ao32_dev_t * dev)
{
    RTSLOG_INFO("Converter Configuration Dump:\n");
    RTSLOG_INFO("Max ADC channels: %u\n", ioread32(&dev->ctrl_mm->max_adc_channels));
    RTSLOG_INFO("Max DAC channels: %u\n", ioread32(&dev->ctrl_mm->max_dac_channels));
    RTSLOG_INFO("Actual num ADC channels: %u\n", ioread32(&dev->ctrl_mm->num_adc_channels));
    RTSLOG_INFO("Actual num DAC channels: %u\n", ioread32(&dev->ctrl_mm->num_dac_channels));
    uint32_t rates_hold = ioread32(&dev->ctrl_mm->max_min_adc_rates);
    RTSLOG_INFO("Supported ADC rates - DMA MAX: %u Hz, DMA MIN: %u Hz, Sample MAX: %u Hz, Sample MIN: %u Hz\n", 
    1<<GET_BYTE(rates_hold, 3), 1<<GET_BYTE(rates_hold, 2), 1<<GET_BYTE(rates_hold, 1), 1<<GET_BYTE(rates_hold, 0));
    rates_hold = ioread32(&dev->ctrl_mm->max_min_dac_rates);
    RTSLOG_INFO("Supported DAC rates - DMA MAX: %u Hz, DMA MIN: %u Hz, Sample MAX: %u Hz, Sample MIN: %u Hz\n", 
    1<<GET_BYTE(rates_hold, 3), 1<<GET_BYTE(rates_hold, 2), 1<<GET_BYTE(rates_hold, 1), 1<<GET_BYTE(rates_hold, 0));
    rates_hold = ioread32(&dev->ctrl_mm->adc_params);
    RTSLOG_INFO("More supported ADC rates - Processing delay: %u Clks?, Native Sampling: %u Hz, MAX Oversample: %u times\n", 
    GET_SHORT(rates_hold, 1), 1<<GET_BYTE(rates_hold, 1), 1<<GET_BYTE(rates_hold, 0));
    rates_hold = ioread32(&dev->ctrl_mm->adc_params);
    RTSLOG_INFO("More supported DAC rates - Processing delay: %u Clks?, Native Sampling: %u Hz, MAX Oversample: %u times\n", 
    GET_SHORT(rates_hold, 1), 1<<GET_BYTE(rates_hold, 1), 1<<GET_BYTE(rates_hold, 0));
    RTSLOG_INFO("ADC DMA buffer sz bytes: %u\n", ioread32(&dev->ctrl_mm->adc_dma_buffer_sz));
    RTSLOG_INFO("DAC DMA buffer sz bytes: %u\n", ioread32(&dev->ctrl_mm->dac_dma_buffer_sz));
    //TODO: Print loopback_and_interface_info
    rates_hold = ioread32(&dev->ctrl_mm->buffs_per_dma_and_clk);
    RTSLOG_INFO("Max buffers per DMA chan: %u, Clk rate: %u Hz\n", 
    1<<GET_BYTE(rates_hold, 1), 1<<GET_BYTE(rates_hold, 0));
    rates_hold = ioread32(&dev->ctrl_mm->dma_buffer_widths);
    RTSLOG_INFO("DAC DMA buffer bus width: %u bytes, DAC DMA buffer sampling width: %u bytes\n "
    "ADC DMA buffer bus width: %u bytes, ADC DMA buffer sampling width: %u bytes\n", 
    1<<GET_BYTE(rates_hold, 3), 1<<GET_BYTE(rates_hold, 2), 1<<GET_BYTE(rates_hold, 1), 1<<GET_BYTE(rates_hold, 0));
    RTSLOG_INFO("AXI Clk: %u Hz\n", ioread32(&dev->ctrl_mm->axi_clk_rate_hz));
    RTSLOG_INFO("DAC data delay: %u\n", ioread32(&dev->ctrl_mm->dac_data_delay));
}

void ligo28ao32_print_adc_dma_settings(ligo28ao32_dev_t * dev)
{
    uint32_t dma_buffer_lens = ioread32(&dev->ctrl_mm->dma_buffer_widths);
    uint32_t LogAdcSamplingWdith = dma_buffer_lens & 0xFF;
    uint32_t LogAdcDmaBusWdith   = (dma_buffer_lens >> 8) & 0xFF;
    //uint32_t LogDacSamplingWdith = (dma_buffer_lens >> 16) & 0xFF;
    //uint32_t LogDacDmaBusWdith   = (dma_buffer_lens >> 24) & 0xFF;
    RTSLOG_INFO("**** ADC DMA Settings ****\n");
    RTSLOG_INFO("READ Only:\n");
    RTSLOG_INFO("ADC DMA Buffer size (bytes): %u\n", ioread32(&dev->ctrl_mm->adc_dma_buffer_sz));
    RTSLOG_INFO("log2 of ADC sample width: %u, log2 of ADC DMA Bus width: %u\n", LogAdcSamplingWdith, LogAdcDmaBusWdith);


    RTSLOG_INFO("RW Registers:\n");
    RTSLOG_INFO("EVEN:\n");
    RTSLOG_INFO("Dest addr <msb>-<lsb> : %x-%x\n", 
           ioread32(&dev->ctrl_mm->dma_chan_config.even_in.addr_msb),
           ioread32(&dev->ctrl_mm->dma_chan_config.even_in.addr_lsb));
    RTSLOG_INFO("Length (bytes): %u\n", ioread32(&dev->ctrl_mm->dma_chan_config.even_in.length));
    RTSLOG_INFO("Offset (bytes): %u\n", ioread32(&dev->ctrl_mm->dma_chan_config.even_in.buffer_offset));
    RTSLOG_INFO("ODD:\n");
    RTSLOG_INFO("Dest addr <msb>-<lsb> : %x-%x\n", 
           ioread32(&dev->ctrl_mm->dma_chan_config.odd_in.addr_msb),
           ioread32(&dev->ctrl_mm->dma_chan_config.odd_in.addr_lsb));
    RTSLOG_INFO("Length (bytes): %u\n", ioread32(&dev->ctrl_mm->dma_chan_config.odd_in.length));
    RTSLOG_INFO("Offset (bytes): %u\n", ioread32(&dev->ctrl_mm->dma_chan_config.odd_in.buffer_offset));
}

void ligo28ao32_dump_ligo_regs(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t num_regs)
{
    char* reg_ptr = (char*)dev->ctrl_mm + offset;
    char* end_ptr = reg_ptr + num_regs*4;
    while (reg_ptr < end_ptr)
    {
        RTSLOG_INFO("Offset: 0x%x, Value: 0x%x\n", offset, ioread32(reg_ptr));
        reg_ptr += 4;
        offset += 4;
    }
}

bool ligo28ao32_read_reg(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t * value)
{

    if (offset >= dev->control_regs_len)
    {
        RTSLOG_ERROR("ligo28ao32_read_reg() : Offset of %d is too big, buffer size %d\n", 
                     offset, dev->control_regs_len);
        return false; 
    }

    *value = *((uint32_t*)(((char*)dev->ctrl_mm) + offset));
    return true;
}

bool ligo28ao32_write_reg(ligo28ao32_dev_t * dev, uint32_t offset, uint32_t value)
{
    if (offset >= dev->control_regs_len)
    {
        RTSLOG_ERROR("ligo28ao32_write_reg() : Offset of %d is too big, buffer size %d\n", 
                     offset, dev->control_regs_len);
        return false; 
    }

    *((uint32_t*)(((char*)dev->ctrl_mm) + offset)) = value;
    return true;
}

void ligo28ao32_print_dma_buffer_status(ligo28ao32_dev_t * dev, uint32_t status)
{
    RTSLOG_INFO("\nDMA Buffer Status:\n");
    RTSLOG_INFO("ADCs Enabled and Running: %u\n", GET_BIT(status, DMA_STAT_ADCS_RUNNING) );
    RTSLOG_INFO("DMA for ADCs is running: %u\n", GET_BIT(status, DMA_STAT_ADC_DMA_RUNNING) );
    RTSLOG_INFO("ADC data valid: %u\n", GET_BIT(status, DMA_STAT_ADC_DATA_VAILD) );
    RTSLOG_INFO("Timing OK: %u\n", GET_BIT(status, DMA_STAT_TIMING_GOOD) );
    RTSLOG_INFO("DMA channel 0 isn't ready: %u\n", GET_BIT(status, DMA_STAT_ADC_DMA_CHAN_0_NOT_READY) );
    RTSLOG_INFO("DMA channel 1 isn't ready: %u\n", GET_BIT(status, DMA_STAT_ADC_DMA_CHAN_1_NOT_READY) );
    RTSLOG_INFO("DMA data in channel 0 didn't arrive in time: %u\n", GET_BIT(status, DMA_STAT_ADC_DMA_CHAN_0_NO_DATA) );
    RTSLOG_INFO("DMA data in channel 1 didn't arrive in time: %u\n", GET_BIT(status, DMA_STAT_ADC_DMA_CHAN_1_NO_DATA) );
    RTSLOG_INFO("DACs are enabled and running: %u\n", GET_BIT(status, DMA_STAT_DACS_RUNNING) );
    RTSLOG_INFO("DMA for DACs is running: %u\n", GET_BIT(status, DMA_STAT_DAC_DMA_RUNNING) );
    RTSLOG_INFO("DAC data valid: %u\n", GET_BIT(status, DMA_STAT_DAC_DATA_VALID) );
    RTSLOG_INFO("Watchdog monitor: %u\n", GET_BIT(status, DMA_STAT_DAC_WATCHDOG) );
    RTSLOG_INFO("DMA channel 0 isn't ready: %u\n", GET_BIT(status, DMA_STAT_DAC_DMA_CHAN_0_NOT_READY) );
    RTSLOG_INFO("DMA channel 1 isn't ready: %u\n", GET_BIT(status, DMA_STAT_DAC_DMA_CHAN_1_NOT_READY) );
    RTSLOG_INFO("DMA data in channel 0 didn't arrive in time: %u\n", GET_BIT(status, DMA_STAT_DAC_DMA_CHAN_0_NO_DATA) );
    RTSLOG_INFO("DMA data in channel 1 didn't arrive in time: %u\n", GET_BIT(status, DMA_STAT_DAC_DMA_CHAN_1_NO_DATA) );
    RTSLOG_INFO("DMA timestamp error in channel 0: %u\n", GET_BIT(status, DMA_STAT_DAC_DMA_CHAN_0_TIME_ERR) );
    RTSLOG_INFO("DMA timestamp error in channel 1: %u\n\n", GET_BIT(status, DMA_STAT_DAC_DMA_CHAN_1_TIME_ERR) );
}


void ligo28ao32_print_samp_status_reg( uint32_t samp_status )
{
    RTSLOG_INFO("Sampling Status (Read Only):\n");
    RTSLOG_INFO("DAC timestamp error chan 1: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_TIMESTAMP_ERR_CHAN_1) );
    RTSLOG_INFO("DAC timestamp error chan 0: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_TIMESTAMP_ERR_CHAN_0) );
    RTSLOG_INFO("DAC DMA missing data 1: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_DMA_MISSING_DATA_1) );
    RTSLOG_INFO("DAC DMA missing data 0: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_DMA_MISSING_DATA_0) );
    RTSLOG_INFO("DAC DMA ready error 1: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_DMA_ERR_CHAN_1) );
    RTSLOG_INFO("DAC DMA ready error 0: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_DMA_ERR_CHAN_0) );
    RTSLOG_INFO("Watchdog Mon: %u\n", GET_BIT(samp_status, SAMP_STAT_WATCHDOG_MON) );
    RTSLOG_INFO("Valid DAC sampling config: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_VALID_SAMP_CONFIG));
    RTSLOG_INFO("DAC converter running: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_CONVERT_RUNNING));
    RTSLOG_INFO("DAC DMA running: %u\n", GET_BIT(samp_status, SAMP_STAT_DAC_DMA_RUNNING));

    RTSLOG_INFO("ADC DMA missing data 1: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_DMA_MISSING_DATA_1) );
    RTSLOG_INFO("ADC DMA missing data 0: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_DMA_MISSING_DATA_0) );
    RTSLOG_INFO("ADC DMA ready error 1: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_DMA_ERR_CHAN_1) );
    RTSLOG_INFO("ADC DMA ready error 0: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_DMA_ERR_CHAN_0) );
    RTSLOG_INFO("ADC Timing OK: %u\n", GET_BIT(samp_status, SAMP_STAT_TIMING_OK) );
    RTSLOG_INFO("Valid ADC sampling config: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_VALID_SAMP_CONFIG) );
    RTSLOG_INFO("ADC converter running: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_CONVERT_RUNNING) );
    RTSLOG_INFO("ADC DMA running: %u\n", GET_BIT(samp_status, SAMP_STAT_ADC_DMA_RUNNING) );
}