#include "ligoPTC.h"
#include "ligo_fw_utils.h"
#include "ligo28ao32.h" 
#include "ligo28ao32_private.h" 

//rts-logger
#define RTS_LOG_PREFIX "ligoPTC"
#include "drv/rts-logger.h"

#include <linux/slab.h>
#include <linux/pci.h>


// PCI identifiers
#define LPTC_VENDOR_ID    0x10ee
#define LPTC_DEVICE_ID    0xd8c6


//Status and Interrupt Control Masks
#define STAT_INT_CTRL_LOCKED_MASK 0x80000000
#define STAT_INT_CTRL_LEAP_SECS_MASK 0xFF



typedef struct ligoPTC_dev_t {
    struct pci_dev * pci_dev;
    LPTC_REGISTER * card_mm;
    uint32_t control_regs_len;
} ligoPTC_dev_t;


ligoPTC_dev_t * ligoPTC_configureDevice(unsigned instance)
{
    int pci_status;
    ligoPTC_dev_t * ctx = ( ligoPTC_dev_t *) kzalloc(sizeof( ligoPTC_dev_t ), GFP_KERNEL);
    if( !ctx ) {
        RTSLOG_INFO("ligoPTC_configureDevice(): Failed to allocate context\n");
        return NULL;
    }

    //Search for LIGO timing card on the PCI bus
    struct pci_dev * tmp_pci_dev = NULL;
    int cur_inst = -1;
    while ( (tmp_pci_dev = pci_get_device(LPTC_VENDOR_ID, LPTC_DEVICE_ID, tmp_pci_dev)) )
    {
        ++cur_inst;
        if( cur_inst == (int)instance) {
            ctx->pci_dev = tmp_pci_dev;
            RTSLOG_INFO("ligoPTC_configureDevice(): Found the device %x:%x driver: %llx\n", tmp_pci_dev->vendor, 
                                                        tmp_pci_dev->device, 
                                                        (uint64_t)tmp_pci_dev->driver);
            break;
        }
    }
    if ( !ctx->pci_dev ) {
        RTSLOG_INFO("ligoPTC_configureDevice(): Did not find instance %u on the PCI bus, cur_inst: %d\n", instance, cur_inst);
        goto cleanup_alloc;
    }


    pci_status = pci_enable_device( ctx->pci_dev );
    if ( pci_status != 0 ) {
        dev_err(&(ctx->pci_dev->dev), "pci_enable_device\n");
        goto cleanup_alloc;
    }

    if (pci_request_region(ctx->pci_dev, BAR0_INDX, "LPTC_region0")) {
		dev_err(&(ctx->pci_dev->dev), "pci_request_region\n");
		goto disable_dev;
	}

    //Map registers
    ctx->control_regs_len = pci_resource_len(ctx->pci_dev, BAR0_INDX);
    ctx->card_mm = pci_iomap(ctx->pci_dev, BAR0_INDX, ctx->control_regs_len);

    /*
    RTSLOG_INFO("ligoPTC_configureDevice(): Timing card time is %llu ns\n", ligoPTC_get_time_ns(ctx));
    RTSLOG_INFO("ligoPTC_configureDevice(): Timing card time is_locked(): %d \n", ligoPTC_is_locked(ctx));
    RTSLOG_INFO("ligoPTC_configureDevice(): ligoPTC_set_slot_config(): ret %d\n", ligoPTC_set_slot_config(ctx, 1, LPTC_SCR_TIM_SIG | LPTC_SCR_LVDS));
    */

    return ctx; //Nominal return


    //
    // Start error returns
    disable_dev:
    if( pci_is_enabled( ctx->pci_dev ) ) pci_disable_device(ctx->pci_dev);
    pci_release_region(ctx->pci_dev, BAR0_INDX);
    cleanup_alloc:
    kfree(ctx);
    return NULL;

}

void ligoPTC_freeDevice(ligoPTC_dev_t * ctx)
{

    if( pci_is_enabled( ctx->pci_dev ) ) pci_disable_device(ctx->pci_dev);

    pci_release_region(ctx->pci_dev, BAR0_INDX);

    kfree(ctx);
    RTSLOG_INFO("ligoPTC_freeDevice() complete.\n");
}

uint64_t ligoPTC_get_time_ns(ligoPTC_dev_t * dev)
{
    struct ligo28ao32_ctrl_regs * regs_ptr = (struct ligo28ao32_ctrl_regs *) dev->card_mm;
    uint32_t frac_ns = convert_4GHz_to_ns( ioread32(&regs_ptr->time_frac) );
    return ((uint64_t)ioread32(&regs_ptr->time_sec) * 1e9) + frac_ns;
}

uint32_t ligoPTC_get_fw_version(struct ligoPTC_dev_t * dev)
{
    struct LPTC_REGISTER * regs_ptr = (struct LPTC_REGISTER *) dev->card_mm;
    return ioread32(&regs_ptr->revision);
}

int ligoPTC_is_locked(ligoPTC_dev_t * dev)
{
    struct ligo28ao32_ctrl_regs * regs_ptr = (struct ligo28ao32_ctrl_regs *) dev->card_mm;
    return (ioread32(&regs_ptr->status_bits) & STAT_INT_CTRL_LOCKED_MASK) > 0;
}

int ligoPTC_set_slot_config(ligoPTC_dev_t * dev, int index, int config)
{
    if(index > LPTC_BP_SLOTS || index < 0) return -1;

    iowrite32(config, &dev->card_mm->slot_info[index].config);
    return 0;
}

void ligoPTC_set_bp_config(struct ligoPTC_dev_t * dev, int config)
{
    struct LPTC_REGISTER * regs_ptr = (struct LPTC_REGISTER *) dev->card_mm;
    iowrite32(config, &regs_ptr->bp_config);

    /*
    for(int i=0; i < 10; ++i)
    {
        RTSLOG_INFO("Slot %d config: 0x%x\n", i, regs_ptr->slot_info[i].config);
        //clearBits(LPTC_SCR_CLK_ENABLE, &regs_ptr->slot_info[i].config);
        iowrite32(0, &regs_ptr->slot_info[i].config);
    }*/


}

bool ligoPTC_read_reg(struct ligoPTC_dev_t * dev, uint32_t offset, uint32_t * value)
{
    if (offset >= dev->control_regs_len)
    {
        RTSLOG_ERROR("ligoPTC_read_reg() : Offset of %d is too big, buffer size %d\n", 
                     offset, dev->control_regs_len);
        return false; 
    }

    *value = *((uint32_t*)(((char*)dev->card_mm) + offset));
    return true;
}

bool ligoPTC_write_reg(struct ligoPTC_dev_t * dev, uint32_t offset, uint32_t value)
{
    if (offset >= dev->control_regs_len)
    {
        RTSLOG_ERROR("ligoPTC_write_reg() : Offset of %d is too big, buffer size %d\n", 
                     offset, dev->control_regs_len);
        return false; 
    }

    *((uint32_t*)(((char*)dev->card_mm) + offset)) = value;
    return true;
}

void ligoPTC_print_card_config(struct ligoPTC_dev_t * dev)
{
    RTSLOG_INFO("\n DUMP CARD CONFIG: \n");
    struct LPTC_REGISTER * regs_ptr = (struct LPTC_REGISTER *) dev->card_mm;
    //RTSLOG_INFO("GPS Time sec/ticks %llu\n", ioread32(&regs_ptr->status_bits));
    RTSLOG_INFO("status: 0x%x\n", ioread32(&regs_ptr->status));
    RTSLOG_INFO("revision: 0x%x\n", ioread32(&regs_ptr->revision));
    RTSLOG_INFO("bp_config: 0x%x\n", ioread32(&regs_ptr->bp_config));
    RTSLOG_INFO("wd_reset: 0x%x\n", ioread32(&regs_ptr->wd_reset));
    RTSLOG_INFO("bp_status: 0x%x\n", ioread32(&regs_ptr->bp_status));
    for(int i=0; i < LPTC_BP_SLOTS; ++i)
    {
        RTSLOG_INFO("\nslot [%d] config: 0x%x\n", i, ioread32(&regs_ptr->slot_info[i].config ));
        RTSLOG_INFO("slot [%d] phase: 0x%x\n", i, ioread32(&regs_ptr->slot_info[i].phase ));
        RTSLOG_INFO("slot [%d] status: 0x%x\n", i, ioread32(&regs_ptr->slot_info[i].status ));
    }



}
