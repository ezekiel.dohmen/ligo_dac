# LIGO Converter Driver
This repository stores linux drivers, for controlling LIGO converter boards. 

| Converter Name | Function | Notes |
| -------------- | -------- | ----- |
| ligo28ao32 | 28bit, 32 channel DAC |  |
| ligo24ai32 | 24bit, 32 channel ADC |  |
| ligoPTC    | LIGO Timing Card      | LIGO PCIe Timing Card, needed to configure timing to slots with other LIGO ADC/DACs | 

All relative paths are from the repository root directory.  

## Using the Userspace Driver/Library

The userspace driver allows you to write an application against the interface described in [`usr_lib/LIGO_FW_Base.hh`](usr_lib/LIGO_FW_Base.hh). It exposes DACs/Timing Cards found on the PCIe bus as devices and the `LIGO_Dac`/`LIGO_PTC` classes will interact with the devices requested by instance number. Instance 0 will be the first card of that type found on the bus, 1 the next and so on.

### Build and Insert Module

```bash
cd usr_driver/
make

#Insert build module
sudo insmod ligo_hw_usr_driver.ko
```

When the driver is inserted, it attempts to take control over all DAC/Timing Cards found on the PCIe bus. You can view the 
devices found by looking for devices created:
```bash
ls /dev/ligo28ao32-*
/dev/ligo28ao32-0  /dev/ligo28ao32-1  /dev/ligo28ao32-2  /dev/ligo28ao32-3
```
Above is an example of a system with 4 DACs present on the system. If no devices are present you can check for logs in `dmesg`:
```bash
sudo dmesg 
```
The module should not insert if no DACs or Timing Cards are found on the bus, so if the `insmod` command fails, you can look for more information in `dmesg`.

### Running Userspace Code
After the kernel module has been inserted and at least one device has been found, you can use the library/programs in `usr_lib/` 
to perform register reads and writes on the device. 

```bash
cd usr_lib/
make #Build library and test applications
./build/ligoDac_example #Run an example program
```

Other applications can link with `LIGO_Hardware.a` library that is build in this directory. 



## The real-time Driver
The `rt_driver/` takes control of a DAC and exercises the DAC/ADC DMA interfaces, recording stats about data flow timing. It does so on a LIGO isolated core, and is dependent on the `advligorts-cpu-isolator-dkms` and `advligorts-logger-dkms` packages. 

### Enabling Repositories and Installed Packages
This [link](https://git.ligo.org/cds/software/advligorts/-/wikis/Enabling-Debian-CDS-Software-Repositories) will show you how to add the LIGO CDS repositories. 

After that has been done, you can install the required packages:
```bash
sudo apt install advligorts-cpu-isolator-dkms advligorts-logger-dkms
```

### Building/Running the real-time Driver

```bash
cd rt_driver/
make

sudo insmod ld32_driver.ko
#Wait for how ever long you want, DAC DMAs are being exercised

#Remove module to stop test
sudo rmmod insmod ld32_driver.ko

#View results
sudo dmesg
```
