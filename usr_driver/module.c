#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/stddef.h>

#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/mutex.h>

//rts-logger
#define RTS_LOG_PREFIX "ligo_hw_usr_driver"
#include "drv/rts-logger.h"


#include "ligo28ao32.h"
#include "ligo28ao32_private.h"
#include "ligofw_ioctl.h"
#include "ligo_fw_utils.h"
#include "ligoPTC.h"

#define MAX_DEVICES 10

//struct mutex g_opened_mutex[MAX_DEVICES];
//struct ligo28ao32_dev_t * g_dac_dev[MAX_DEVICES] = {NULL,};
//struct ligoPTC_dev_t * g_lptc_dev[MAX_DEVICES] = {NULL,};

typedef enum LIGO_CARD_t {
    LIGO_28AO32,
    LIGO_PTC
} LIGO_CARD_t;

struct card_info_t {
    LIGO_CARD_t card_type;
    union driver_t {
        void * v_ptr;
        struct ligo28ao32_dev_t * l28Dac;
        struct ligoPTC_dev_t * lptc;
    } driver;
    bool (*write_reg)(void * dev, uint32_t offset, uint32_t value);
    bool (*read_reg)(void * dev, uint32_t offset, uint32_t * value);
    struct mutex in_use_mutex;
};
struct card_info_t g_card_info[MAX_DEVICES];
static int g_num_devs = 0;



static int ligo_hw_open(struct inode *inode, struct file *file);
static int ligo_hw_release(struct inode *inode, struct file *file);
static long ligo_hw_ioctl(struct file *file, unsigned int cmd, unsigned long arg);


static const struct file_operations ligo_hw_fops = {
    .owner      = THIS_MODULE,
    .open       = ligo_hw_open,
    .release    = ligo_hw_release,
    .unlocked_ioctl = ligo_hw_ioctl,
};

struct ligo_hw_device_data_t {
    struct cdev cdev;
};

static const int g_dev_major = 133;
static const int g_dev_minor = 0;
static dev_t g_ld32_dev = MKDEV(g_dev_major, g_dev_minor);
static struct class *g_ligo_hw_class = NULL;
static struct ligo_hw_device_data_t ligo_hw_data[MAX_DEVICES];

static int ligo_hw_uevent(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

static int find_all_devices( void )
{
    int num_dacs = 0, num_lptcs = 0, cur_index = 0;

    while ( (g_card_info[cur_index].driver.l28Dac = ligo28ao32_configureDevice( num_dacs )) && cur_index < MAX_DEVICES)
    {
        g_card_info[cur_index].card_type = LIGO_28AO32;
        g_card_info[cur_index].write_reg = (bool (*)(void *, uint32_t, uint32_t))&ligo28ao32_write_reg;
        g_card_info[cur_index].read_reg  = (bool (*)(void *, uint32_t, uint32_t*))&ligo28ao32_read_reg;
        ++num_dacs; 
        ++cur_index;
    }

    while ( (g_card_info[cur_index].driver.lptc = ligoPTC_configureDevice( num_lptcs )) && cur_index < MAX_DEVICES )
    {
        g_card_info[cur_index].card_type = LIGO_PTC;
        g_card_info[cur_index].write_reg = (bool (*)(void *, uint32_t, uint32_t))&ligoPTC_write_reg;
        g_card_info[cur_index].read_reg  = (bool (*)(void *, uint32_t, uint32_t*))&ligoPTC_read_reg;
        ++num_lptcs;
        ++cur_index;
    }

    RTSLOG_INFO("Successfully brought up %d ligo28ao32 devices and %d ligoPTCs\n", num_dacs, num_lptcs);
    return num_dacs + num_lptcs;
}


static int __init ligo_hw_init(void) 
{


    int num_cards = find_all_devices();
    if (num_cards <= 0) {
        RTSLOG_ERROR("Module insert could not find any LIGO FW loaded cards to create devices for. Exiting\n");
        return -ENODEV;
    }

    for(int i=0; i < num_cards; ++i)
        mutex_init( &g_card_info[i].in_use_mutex );

    if ( alloc_chrdev_region(&g_ld32_dev, 0, num_cards, "ligoFW") < 0 ) {
        RTSLOG_ERROR("Could not alloc_chrdev_region()\n");
        return -EBUSY;
    }

    if ( IS_ERR(g_ligo_hw_class = class_create(THIS_MODULE, "ligoFW")) ) {
        RTSLOG_ERROR("Could not class_create() with name: ligoFW\n");
        goto clean_device;
    }
    g_ligo_hw_class->dev_uevent = ligo_hw_uevent;

    int last_init = -1, num_dacs=0, num_lptcs=0;
    char dev_name_buf[100];
    for (int i=0; i < num_cards; ++i)
    {
        cdev_init(&ligo_hw_data[i].cdev, &ligo_hw_fops);
        ligo_hw_data[i].cdev.owner = THIS_MODULE;

        if ( cdev_add(&ligo_hw_data[i].cdev, MKDEV(g_dev_major, i), 1) < 0 ) {
            RTSLOG_ERROR("cdev_add() failed : Cannot add the device to the system\n");
            goto clean_cdev;
        }

        if ( g_card_info[i].card_type == LIGO_28AO32 )
        {
            snprintf(dev_name_buf, 100, "ligo28ao32-%d", num_dacs);
            ++num_dacs;
        }
        else if ( g_card_info[i].card_type == LIGO_PTC)
        {
            snprintf(dev_name_buf, 100, "ligoPTC-%d", num_lptcs);
            ++num_lptcs;
        }
        if ( IS_ERR(device_create(g_ligo_hw_class, NULL, MKDEV(g_dev_major, i), NULL, dev_name_buf)) ) {
            RTSLOG_ERROR("device_create() failed, Cannot create device %d\n", i);
            cdev_del(&ligo_hw_data[i].cdev); //Clean up the above command
            goto clean_cdev;
        }
        last_init = i;
        g_num_devs++;
    }

    RTSLOG_INFO("Driver insert done.\n");
    return 0; //END of nominal case


clean_cdev:
    for(int i=last_init; i>=0; --i)
    {
        device_destroy(g_ligo_hw_class, MKDEV(g_dev_major, i));
        cdev_del(&ligo_hw_data[i].cdev);
    }
//clean_class:
    class_destroy(g_ligo_hw_class);
clean_device:
    unregister_chrdev_region(g_ld32_dev, MINORMASK);

    return -EBUSY;
}


static void __exit ligo_hw_exit(void) 
{

    for(int i=0; i < g_num_devs; ++i)
    {
        device_destroy(g_ligo_hw_class, MKDEV(g_dev_major, i));
        cdev_del(&ligo_hw_data[i].cdev);
    }
    class_destroy(g_ligo_hw_class);
    unregister_chrdev_region(g_ld32_dev, MINORMASK);

    for(int i=0; i < g_num_devs; ++i) {
        if( g_card_info[i].card_type == LIGO_28AO32 ) {
            ligo28ao32_dma_stop_adcs(g_card_info[i].driver.l28Dac);
            ligo28ao32_dma_stop_dacs(g_card_info[i].driver.l28Dac);
            udelay(19999); //Waited for any started transfer
            ligo28ao32_freeDevice( g_card_info[i].driver.l28Dac );
            g_card_info[i].driver.l28Dac = NULL;
        }
        else if ( g_card_info[i].card_type == LIGO_PTC)
        {
            ligoPTC_freeDevice(g_card_info[i].driver.lptc);
            g_card_info[i].driver.lptc = NULL;
        }
        else
        {
            RTSLOG_ERROR("Unsupported card type found when cleaning up module.\n");
        }
    }

	RTSLOG_INFO("Unloaded ligo_hw driver!\n");
}



static int ligo_hw_open(struct inode *inode, struct file *file)
{
    //printk("ligo_hw: Device open, dev index: %d\n", MINOR(file->f_path.dentry->d_inode->i_rdev));
    int dev_index = MINOR(file->f_path.dentry->d_inode->i_rdev);
    if ( mutex_trylock( &g_card_info[dev_index].in_use_mutex ) != 1)
    {
        RTSLOG_ERROR("This device has already been open by another process.\n");
        return -EBUSY;
    }

    return 0;
}

static int ligo_hw_release(struct inode *inode, struct file *file)
{
    int dev_index = MINOR(file->f_path.dentry->d_inode->i_rdev);
    mutex_unlock( &g_card_info[dev_index].in_use_mutex );

    return 0;
}

int handle_read_regs(ligo_ioctl_read_regs_t cmd, int dev_index)
{
    uint32_t reg;

    for (int i=0; i < cmd.num_regs; ++i)
    {
        if ( g_card_info[dev_index].read_reg(g_card_info[dev_index].driver.v_ptr, 
                                             cmd.addr_offset, 
                                             &reg) != true) return -EINVAL;
        if ( copy_to_user((int32_t*) &cmd.vals[i], &reg, sizeof(reg)) )
        {
            RTSLOG_ERROR("Failed to copy register at offset 0x%x to userspace\n", cmd.addr_offset);
            return -EINVAL;
        }
        cmd.addr_offset += sizeof(uint32_t);
    }
    return 0;
}

int handle_write_regs(ligo_ioctl_write_regs_t cmd, int dev_index)
{
    uint32_t reg;

    for (int i=0; i < cmd.num_regs; ++i)
    {
        if ( copy_from_user((int32_t*) &reg, &cmd.vals[i], sizeof(reg)) )
        {
            RTSLOG_ERROR("Failed to copy register value at index %d from userspace\n", i);
            return -EINVAL;
        }

        if ( g_card_info[dev_index].write_reg(g_card_info[dev_index].driver.v_ptr, 
                                              cmd.addr_offset, 
                                              reg) != true) return -EINVAL;
        cmd.addr_offset += sizeof(uint32_t);
    }
    return 0;
}

static long ligo_hw_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int dev_index = MINOR(file->f_path.dentry->d_inode->i_rdev);
    //printk("ligo_hw: Device ioctl, dev_index: %d\n", dev_index);
    union ligo_any_cmd_t command_store;

    switch ( cmd ) {
        case LIGOFW_WRITE_REGS:
            if ( copy_from_user(&command_store.write_cmd, 
                                (int32_t*) arg, 
                                sizeof(command_store.write_cmd)) )
            {
                RTSLOG_ERROR("Could not copy write command from user pointer.\n");
                return -EFAULT;
            }
            return handle_write_regs(command_store.write_cmd, dev_index);
            break;
        case LIGOFW_READ_REGS:

            if ( copy_from_user(&command_store.read_cmd, 
                                (int32_t*) arg, 
                                sizeof(command_store.read_cmd)) )
            {
                RTSLOG_ERROR("Could not copy read command from user pointer.\n");
                return -EFAULT;
            }
            return handle_read_regs(command_store.read_cmd, dev_index);
            break;
        default:
            pr_info("Default\n");
            break;
    }


    return 0;
}



module_init(ligo_hw_init);
module_exit(ligo_hw_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("LIGO");
MODULE_DESCRIPTION("Userspace IOCLT interface for LIGO DAC PCIe card");